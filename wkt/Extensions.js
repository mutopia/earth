/**
 * Well-Known Text Utility
 * Uses subsets of Open Layers library
 * @author aramk
 */

define([
  'dojo/_base/declare',
  '../lib/open-layers'
], function (declare, OpenLayers) {

  var instance = null;

  var Extensions = declare(null, {
    // summary:
    //      A set of utility methods for working with Well-Known-Text

    constructor:function () {
      this.parser = new OpenLayers.Format.WKT();
    },

    // TODO rename to wktTo
    wktToCoords:function (/*String*/ poly) {

      return this.wktToArray(poly);

      // summary:
      //      Returns an array of coordinates from the WKT Polygon string
      // description:
      //      e.g. "POLYGON ((-37.760746002197266 145.15419006347656, ...))"

      var vector = this.parser.read(poly);

      // 2D array of coordinates
      var coords2D = [];

      var geometry = vector.geometry;

      if (geometry instanceof OpenLayers.Geometry.Point) {
        var vertices = geometry.getVertices();
        coords2D.push(vertices);
      } else if (geometry instanceof OpenLayers.Geometry.Polygon) {
        var components = geometry.components;
        for (var i = 0; i < components.length; i++) {
          var component = components[i];
        }
      }
      return coords2D;
    },

    parseGeometry:function (wktStr) {
      var geometry = wktStr;
      if (typeof geometry == 'string') {
        geometry = this.parser.read(wktStr);
      }
      return geometry;
    },

    wktToArray:function (wktStr) {
      var geometry = this.parseGeometry(wktStr).geometry;
      return this.wktGeometryToArray(geometry);
    },

    wktGeometryToArray:function (geometry) {
      if (geometry instanceof OpenLayers.Geometry.Point) {
        return this.wktPointToArray(geometry);
      } else if (geometry instanceof OpenLayers.Geometry.Collection) {
        var coords = [];
        var components = geometry.components;
        for (var i = 0; i < components.length; i++) {
          var component = components[i];
          coords.push(this.wktGeometryToArray(component));
        }
      }
      return coords;
    },

    wktPointToArray:function (point) {
      return [point.x, point.y];
    },

    coordsToWKTObject:function (/*Array*/ coords) {
      // summary:
      //      Returns a WKT Polygon from an array of coordinates
      var points = this.toPoints(coords);
      var ring = new OpenLayers.Geometry.LinearRing(points);
      var len = ring.components.length;
      if (ring.components.length > 1) {
        ring.components.pop();
      }
      if (ring.components[0] != ring.components.slice(-1)) {
        ring.components.push(ring.components[0]);
      }
      return new OpenLayers.Geometry.Polygon([ring]);
    },

    coordsToWKT:function (/*Array*/ coords) {
      // summary:
      //      Returns a WKT string from an array of coordinates
      return this.wktObjectToString(this.coordsToWKTObject(coords));
    },

    wktObjectToString:function (/*Object*/ obj) {
      // summary:
      //      Returns a WKT string from a WKT object
      return this.parser.extractGeometry(obj);
    },

    toPoints:function (/*Array*/ coords) {
      // summary:
      //      Converts an array of coordinates into an array of Points
      var points = [];
      for (var i = 0; i < coords.length; i++) {
        var coord = coords[i];
        points.push(new OpenLayers.Geometry.Point(coord[0], coord[1]));
      }
      return points;
    }

  });

  return (instance = (instance || new Extensions()));

});
