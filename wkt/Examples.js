define([
  'dojo/_base/declare',
  './Extensions',
  'utility/Log'
], function (declare, Extensions, Log) {
  return declare(null, {
    constructor:function () {
      Log.info('./wkt/Examples');
      var poly = "POLYGON ((1 2, 3 4))";
      Log.info(poly);
      // TODO update to latest functions
      var coords = Extensions.wktToCoords(poly);
      var output = Extensions.coordsToWKT(coords);
      Log.info(output);
    }
  });
});