define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/Color',
  'dojo/request/xhr',
  './Geocoder',
  './wkt/Extensions',
  'utility/Log'
], function (declare, lang, Color, xhr, Geocoder, wkt, Log) {

//  var time

//  var basis = (new Date()).getTime();
  var first = null;
  var last = null;
  var timer = function() {
    var time = (new Date()).getTime();
    if (!last) {
      last = time;
    }
    if (!first) {
      first = time;
    }
    var diff = time - last;
    last = time;
    return diff;
  };

//  console.log('basisTime', basisTime);
//  console.log('time 1', (new Date()).getTime() - basisTime);
//  console.log('time 2', (new Date()).getTime() - basisTime);

  return declare(null, {
    // summary:
    //      Provides methods to test the Earth modules with sample data and code

    gexx: null,
    gex: null,
    ge: null,

    xhrArgs: {
      headers: {Accept: 'application/json'},
      handleAs: 'json'
    },

    constructor: function (gexx) {
      Log.info('./Examples', gexx);
      this.gexx = gexx;
      this.gex = this.gexx.gex;
      this.ge = this.gexx.ge;
      this.init();
    },

    init: function () {

      // TODO convert all this to a test case!

      this.gexx.disableAllLayers();

      window.melbLat = -37.7833;
      window.melbLng = 144.9667;

      window.marLat = -37.77;
      window.marLng = 144.893;
      window.marAlt = 5000;

      this.resourcesURL = window.currentDir + '/static/earth/';
      this.kmlURL = this.resourcesURL + 'kml/';

      // Uncomment these to run them
      var run = [
//                this.zoomMelb,
//                this.zoomMarib,
//                this.geocode,
//                this.drawKML,
//                this.draw2D,
//                this.userDraw2D,
//        this.draw3D,
//        this.drawPrism,
//                this.drawPrismCustom
//                this.drawPrismSimple
//                this.drawKMLModelManual
//                this.drag,
//                this.drawPoint,
//                this.growHeight,
//                this.movePoly,
//                this.edit,
//                this.drawLine,
//                this.showBalloon,
//                this.loadKmlManual
//        this.mapChildren
//        this.drawTimed,
      ];

      setTimeout(lang.hitch(this, function () {
        this.drawTimedKML(1);
      }), 3000);

//      setTimeout(lang.hitch(this, function () {
//        this.drawPrism();
//      }), 3000);

      run.forEach(lang.hitch(this, function (r) {
        lang.hitch(this, r)();
      }));
    },

    zoomMelb: function () {
      this.gexx.setLookAt(melbLat, melbLng, 150000);
    },

    zoomMarib: function () {
      this.gexx.setLookAt(marLat, marLng, marAlt);
    },

    geocode: function () {
      Geocoder.geocode('Melbourne, Australia').then(lang.hitch(this, function (args) {
        var results = args.results;
        if (results.length) {
          var result = results[0];
          Log.info('Geocode', result);
          Log.info('Geocode long_name', Geocoder.getProperty(result, 'long_name'));
          Log.info('Geocode location', Geocoder.getProperty(result, 'location'));
        }
      }));
    },

    drawKML: function () {
      var href = this.kmlURL + "polygon.kml";
      Log.info("Loading polygon from " + href);
      // TODO Handle failure to load KML
      this.gex.util.displayKml(href, {
        flyToView: true
      });
    },

    draw2D: function () {
      var obj = this.gexx.drawPoly2D({
        coords: [
          [melbLat + 0.2, melbLng],
          [melbLat + 0.2, melbLng + 0.1],
          [melbLat + 0.2 + 0.1, melbLng + 0.1]
        ],
        opacity: 0.5,
        lineOpacity: 1
      });

      var obj = this.gexx.drawPoly2D({
        coords: [
          [melbLat + 0.3, melbLng + 0.2],
          [melbLat + 0.3, melbLng + 0.2 + 0.1],
          [melbLat + 0.3 + 0.1, melbLng + 0.2 + 0.1]
        ],
        opacity: 0.5,
        lineOpacity: 1
      });

      Log.info('draw2D', obj);
    },

    userDraw2D: function () {
      this.gexx.userDrawPoly({
        finishCallback: lang.hitch(this, function (poly) {
          this.gexx.userEditPoly(poly);
          this.gexx.addEvent(poly, 'click', lang.hitch(this, function (event) {
            Log.info('poly clicked');
          }));
        }),
        polygon: {
          opacity: 0.5,
          lineOpacity: 1
        }
      });
    },

    drawTimed: function (times) {
      times = times || 1;
      timer();
      var avgRenderTime = 0;
      var poly = null;
      for (var i = 0; i < 20; i++) {
        var polyStr = "POLYGON ((2.9252205810348113 101.6861500590334, 2.9249775462843948 101.68613462328187, 2.9247350300312083 101.68611260557424, 2.9244932134989585 101.6860840211819, 2.9242522752200655 101.68604889076607, 2.9244647470765592 101.68537531959298, 2.9247344908469644 101.68540277031144, 2.9250345957312534 101.68540684686621, 2.925463953058892 101.6861588993541, 2.9252205810348113 101.6861500590334))";
        console.log('str', timer());
        var coords = wkt.wktToArray(polyStr)[0];
        console.log('wkt', timer());

        poly = this.gexx.buildPoly3D({
          coords: coords,
          opacity: 0.5,
          lineOpacity: 1,
          height: 100
        });

        var renderTime = timer();
        avgRenderTime += renderTime;
        console.log('poly', i, renderTime);
      }

      times--;

      if (times > 0) {
        setTimeout(lang.hitch(this, function () {
          this.drawTimed(times);
        }), 2000);
      } else {
        timer();
        console.log('count ' + i, 'took ' + (last - first), 'average ' + (avgRenderTime/i));
        this.gexx.append(poly);
        this.gexx.flyTo(poly);
      }
    },

    drawTimedKML: function (times) {
      times = times || 1;
      timer();
      var avgRenderTime = 0;
      var avgKmlRenderTime = 0;
      var avgStaticRenderTime = 0;
      var avgDynamicRenderTime = 0;
      var avgDynamic2RenderTime = 0;
      var poly = null;

      // Prepare coordinates
      var polyStr = "POLYGON ((2.9252205810348113 101.6861500590334, 2.9249775462843948 101.68613462328187, 2.9247350300312083 101.68611260557424, 2.9244932134989585 101.6860840211819, 2.9242522752200655 101.68604889076607, 2.9244647470765592 101.68537531959298, 2.9247344908469644 101.68540277031144, 2.9250345957312534 101.68540684686621, 2.925463953058892 101.6861588993541, 2.9252205810348113 101.6861500590334))";
      var coords = wkt.wktToArray(polyStr)[0];
      // TODO doesn't work...
      var coordsWithHeight = this.gexx.setAltitude(coords, 100);

      console.log('start', timer());

      for (var i = 0; i < 100; i++) {
        // Legacy rendering
        poly = this.gexx.buildPoly3D({
          coords: coords,
          opacity: 0.5,
          lineOpacity: 1,
          height: 100
        });
        var renderTime = timer();
        avgRenderTime += renderTime;
        console.log('poly', i, renderTime);

        // KML Rendering
        var polyKml = this.gexx.buildPoly3D({
          coords: coords,
          opacity: 0.5,
          lineOpacity: 1,
          height: 100
        });
        var renderTimeKml = timer();
        avgKmlRenderTime += renderTimeKml;
        console.log('poly kml', i, renderTimeKml);

        // KML Static
        var kmlStatic = '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\
<Placemark>\
	<Style>\
		<LineStyle>\
			<color>ff00ff00</color>\
			<width>2</width>\
		</LineStyle>\
		<PolyStyle>\
			<color>7f00ff00</color>\
		</PolyStyle>\
	</Style>\
	<Polygon>\
		<extrude>1</extrude>\
		<tessellate>1</tessellate>\
		<altitudeMode>relativeToGround</altitudeMode>\
		<outerBoundaryIs>\
			<LinearRing>\
				<coordinates>\
					101.6861500590334,2.925220581034812,100 101.6861346232819,2.924977546284394,100 101.6861126055742,2.924735030031208,100 101.6860840211819,2.924493213498959,100 101.6860488907661,2.924252275220065,100 101.685375319593,2.92446474707656,100 101.6854027703114,2.924734490846964,100 101.6854068468662,2.925034595731254,100 101.6861588993541,2.925463953058892,100 101.6861500590334,2.925220581034812,100 101.6861500590334,2.925220581034812,100 \
				</coordinates>\
			</LinearRing>\
		</outerBoundaryIs>\
	</Polygon>\
</Placemark>\
</kml>';
        var kmlStaticPoly = this.ge.parseKml(kmlStatic);
        var staticRenderTime = timer();
        avgStaticRenderTime += staticRenderTime;
        console.log('kml static', i, staticRenderTime);

        // Dynamic Rendering
        var kmlDynamicPoly = this.gex.dom.buildPlacemark({
          polygon: {
            polygon: coordsWithHeight
          }
        });
        var dynamicRenderTime = timer();
        avgDynamicRenderTime += dynamicRenderTime;
        console.log('dynamic', i, dynamicRenderTime);

        // Dynamic Rendering 2
        var polygon = this.ge.createPolygon('');
//        var ring = this.gex.dom.buildLinearRing(coordsWithHeight);
        var outer = this.ge.createLinearRing('');
        polygon.setOuterBoundary(outer);
        var outerCoords = outer.getCoordinates();
        for (var j = 0; j < coordsWithHeight.length; j++) {
          var coord = coordsWithHeight[j];
          // TODO use different push if no height
//          console.log('coord', coord);
          outerCoords.pushLatLngAlt(coord[0], coord[1], coord[2]);
        }
        polygon.setExtrude(true);
        polygon.setTessellate(true);
        polygon.setAltitudeMode(1);
        var placemark = this.ge.createPlacemark('');
        placemark.setGeometry(polygon);
        dynamic2RenderTime = timer();
        avgDynamic2RenderTime += dynamic2RenderTime;
        console.log('dynamic 2', i, dynamic2RenderTime);
      }

      times--;

      if (times > 0) {
        setTimeout(lang.hitch(this, function () {
          this.drawTimed(times);
        }), 2000);
      } else {
        timer();

        this.gexx.append(kmlStaticPoly);
        this.gexx.flyTo(kmlStaticPoly);

        console.log('poly: count ' + i, 'took ' + (last - first), 'average ' + (avgRenderTime/i));
        console.log('poly kml: count ' + i, 'took ' + (last - first), 'average ' + (avgKmlRenderTime/i));
        console.log('static: count ' + i, 'took ' + (last - first), 'average ' + (avgStaticRenderTime/i));
        console.log('dynamic: count ' + i, 'took ' + (last - first), 'average ' + (avgDynamicRenderTime/i));
        console.log('dynamic2: count ' + i, 'took ' + (last - first), 'average ' + (avgDynamic2RenderTime/i));
      }
    },

    draw3D: function () {
      this.poly = this.gexx.drawPoly3D({
        coords: [
          [melbLat + 0.2, melbLng - 0.2],
          [melbLat + 0.2, melbLng + 0.1 - 0.2],
          [melbLat + 0.1 + 0.2, melbLng + 0.1 - 0.2]
        ],
        color: '#ff0',
        opacity: 0.5,
        lineColor: '#f0f',
        lineOpacity: 0.5,
        height: 100
      });

      this.gexx.addEvent(this.poly, 'click', lang.hitch(this, function (event) {
        this.gexx.setSelected(event.getTarget());
      }));

      this.gexx.addEvent(this.ge.getGlobe(), 'click', lang.hitch(this, function (event) {
        this.gexx.setSelected(null);
      }));

      window.newCoords = this.gexx.setAltitude([
        [melbLat + 0.2, melbLng - 0.2],
        [melbLat + 0.2, melbLng + 0.1 - 0.2],
        [melbLat + 0.1 + 0.2, melbLng + 0.1 - 0.2]
      ], 10000);

      this.gexx.setOuterBoundary(this.poly, window.newCoords);

      this.gexx.setHeight(this.poly, 8000);
    },

    mapChildren: function () {
      this.gexx.mapChildren(function (child) {
        console.log('child', child.getType());
      });
    },

    drawPrism: function () {
      timer();
      var originLat = melbLat;
      var originLng = melbLng;
      var originAlt = 1000;
      var altAdd = 1000;
      var prisma = this.gexx.drawPrism({
        coords: [
          [originLat, originLng, originAlt], // origin (bottom-left)
          [originLat, originLng + 0.1, originAlt], // bottom-right
          [originLat + 0.1, originLng + 0.1, originAlt], // top-right
          [originLat + 0.1, originLng, originAlt], // top-left
          [originLat, originLng, originAlt] // origin again (possibly unnecessary)
        ],
        color: '#00f',
        opacity: 0.5,
        lineOpacity: 1,
        height: 1000
      });
      console.log('prism: ', timer());

//      var prismStr = '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\
//<Placemark>\
//	<MultiGeometry>\
//		<Polygon>\
//			<tessellate>1</tessellate>\
//			<altitudeMode>relativeToGround</altitudeMode>\
//			<outerBoundaryIs>\
//				<LinearRing>\
//					<coordinates>\
//						144.9667,-37.7833,6000 145.0667,-37.7833,6000 145.0667,-37.7833,1000 144.9667,-37.7833,1000 144.9667,-37.7833,6000 \
//					</coordinates>\
//				</LinearRing>\
//			</outerBoundaryIs>\
//		</Polygon>\
//		<Polygon>\
//			<tessellate>1</tessellate>\
//			<altitudeMode>relativeToGround</altitudeMode>\
//			<outerBoundaryIs>\
//				<LinearRing>\
//					<coordinates>\
//						145.0667,-37.7833,6000 145.0667,-37.6833,6000 145.0667,-37.6833,1000 145.0667,-37.7833,1000 145.0667,-37.7833,6000 \
//					</coordinates>\
//				</LinearRing>\
//			</outerBoundaryIs>\
//		</Polygon>\
//	</MultiGeometry>\
//</Placemark>\
//</kml>';
//
//      var prism = this.ge.parseKml(prismStr);
//      this.gexx.append(prism);
//      console.log('prism kml: ', timer());

      setTimeout(lang.hitch(this, function () {
        this.gexx.flyTo(prisma);
      }), 2000);
    },

    drawPrismCustom: function () {
      var originLat = melbLat;
      var originLng = melbLng;
      var originAlt = 1000;
      var altAdd = 1000;
      this.poly = this.gexx.drawPrism({
        coords: [
          [originLat, originLng, originAlt - 100],
          [originLat, originLng + 0.1, originAlt + 100],
          [originLat + 0.1, originLng + 0.1, originAlt - 100],
          [originLat + 0.1, originLng, originAlt + 100],
          [originLat, originLng, originAlt - 100 + altAdd],
          [originLat, originLng + 0.1, originAlt + 100 + altAdd * 3],
          [originLat + 0.1, originLng + 0.1, originAlt - 100 + altAdd],
          [originLat + 0.1, originLng, originAlt + 100 + altAdd * 2]
        ],
        opacity: 0.5,
        lineOpacity: 1
      });
      setTimeout(lang.hitch(this, function () {
        this.gexx.flyTo(this.poly);
      }), 2000);
//            Log.info(this.poly.getType());

//            this.gexx.userEditPoly(this.poly);

//            var geometries = this.poly.getGeometry().getGeometries().getChildNodes();
//            for (var i = 0; i < geometries.getLength(); i++) {
//                var geom = geometries.item(i);
//                Log.info('geom', geom.getType());
//                this.gexx.userEditPoly(this.poly, {
//                    line: geom.getOuterBoundary()
//                });
//            }
    },

    drawPrismSimple: function () {
      var originLat = melbLat;
      var originLng = melbLng;
      this.poly = this.gexx.drawPrism({
        coords: [
          [originLat, originLng],
          [originLat, originLng + 0.1],
          [originLat + 0.1, originLng + 0.1],
          [originLat + 0.1, originLng]
        ],
        opacity: 0.5,
        lineOpacity: 1,
        height: 5000,
        altitude: 1000
      });
      setTimeout(lang.hitch(this, function () {
        this.gexx.flyTo(this.poly);
      }), 2000);
    },

    drawKMLModelManual: function () {
      var placemark = this.ge.createPlacemark('');
      placemark.setName('model');
      var model = this.ge.createModel('');
      this.ge.getFeatures().appendChild(placemark);
      var loc = this.ge.createLocation('');
      model.setLocation(loc);
      var link = this.ge.createLink('');

      // A textured model created in Sketchup and exported as Collada.
      link.setHref('http://earth-api-samples.googlecode.com/svn/trunk/examples/' +
          'static/splotchy_box.dae');
      model.setLink(link);

      placemark.setGeometry(model);

      setTimeout(lang.hitch(this, function () {
        var la = this.ge.getView().copyAsLookAt(this.ge.ALTITUDE_RELATIVE_TO_GROUND);
        loc.setLatitude(la.getLatitude());
        loc.setLongitude(la.getLongitude());
        la.setRange(300);
        la.setTilt(45);
        this.ge.getView().setAbstractView(la);

        this.gexx.userEditPoly(placemark);
      }), 2000);

    },

    _loadKmlNetwork: function (href) {
      console.log('href', href);
      var link = this.ge.createLink('');
      link.setHref(href);

      var networkLink = this.ge.createNetworkLink('');
      networkLink.set(link, true, true); // Sets the link, refreshVisibility, and flyToView

      this.ge.getFeatures().appendChild(networkLink);
    },

    _loadKmlFetch: function (href) {
      console.log('href', href);
      google.earth.fetchKml(this.ge, href, lang.hitch(this, function (kmlObject) {
        console.log('kmlObject', kmlObject);
        if (kmlObject) {
          console.log('kmlObject', kmlObject.getType());
          this.ge.getFeatures().appendChild(kmlObject);
        }
      }));
    },

    drag: function () {
      this.gexx.setDragMode(true);
      this.gexx.addDragEvent(this.poly);
    },

    edit: function () {
      // polygon is returned, and also available in the callback when drawing done
      var polygon = this.gexx.userDrawPoly(function (polygon) {
        Log.info(polygon);
      });
    },

    growHeight: function () {
      var poly = this.poly;
      var count = 0;
      var interval = setInterval(lang.hitch(this, function () {
        this.gexx.addPolyCoords(poly, [0, 0, 1000]);
        count++;
        if (count == 10) {
          clearInterval(interval);
        }
      }), 1000);
    },

    movePoly: function () {
      setInterval(lang.hitch(this, function () {
        newCoords = this.gexx.displaceCoords(newCoords, [-0.005, -0.005], function (x, y, i, j) {
          return x + y;
        });

        Log.info(newCoords);

        this.gexx.modifyOuterBoundary(this.poly, newCoords);
      }), 200);
    },

    drawPoint: function () {
      var point = this.gexx.drawPoint({name: "test", point: [melbLat, melbLng]});

      this.gex.edit.makeDraggable(point, {
        //bounce : point,
        dragCallback: function () {
          Log.info('drag');
        }
      });
    },

    drawLine: function () {
      this.gexx.drawLine({
        coords: [
          [0.2 + melbLat, 0.2 + melbLng],
          [0.2 + melbLat, 0.2 + melbLng + 0.1],
          [0.2 + melbLat + 0.1, 0.2 + melbLng + 0.1]
        ],
        lineColor: 'blue'
      });
    },

    showBalloon: function () {
      var content = this.poly.getDescription();
      this.poly.setDescription('This is a <b>test</b>');
      // Longhand
      var balloon = this.gexx.createBalloon(this.poly, 'html', {
        content: content
      });
      // Shorthand
      var balloon = this.gexx.createHTMLBalloon(this.poly, {
        content: content
      });
    }

  });
});
