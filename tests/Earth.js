define([
    'doh/runner',
    '../Earth'
], function (doh, Earth) {

    var tests = [
        {
            name: "initEarth",
            setUp: function () {
            },
            runTest: function () {
                var deferred = new doh.Deferred();

                var div = document.createElement('div');
                document.body.appendChild(div);

                doh.earth = Earth(div, function () {
                    setTimeout(deferred.getTestCallback(function () {
                        doh.assertNotEqual(doh.earth.ge, null);
                    }), 100);
                });

                return deferred;
            },
            tearDown: function () {
            },
            timeout: 10000
        }
    ];

    doh.register('./Earth', tests);

    return tests;

});
