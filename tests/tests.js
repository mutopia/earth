define(['app/manager/TestManager'], function (manager) {
	manager.prototype.runTests('earth', [
		'Earth',
        'Geocoder',
        'Extensions',
        'wkt/Extensions'
	]);
});
