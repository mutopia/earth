define([
    'doh/runner',
    '../../wkt/Extensions'
], function (doh, wkt) {

    var poly = 'POLYGON((1 2,3 4))';
    var coords = [
        [1, 2],
        [3, 4]
    ];
    var points = [
        new OpenLayers.Geometry.Point(coords[0][0], coords[0][1]),
        new OpenLayers.Geometry.Point(coords[1][0], coords[1][1])
    ];

    doh.register('./wkt/Extensions', [
        {
            name: 'toWKT',
            runTest: function () {
                doh.assertEqual(coords, wkt.wktToCoords(poly));
            }
        },
        {
            name: 'toPoints',
            runTest: function () {
                var gen = wkt.toPoints(coords);
                doh.assertTrue(points[0].x == gen[0].x && points[0].y == gen[0].y);
                doh.assertTrue(points[1].x == gen[1].x && points[1].y == gen[1].y);
                doh.assertEqual(points.length, gen.length);
            }
        },
        {
            name: 'toPolygon',
            runTest: function () {
                doh.assertEqual(poly, wkt.coordsToWKT(coords));
            }
        }
    ]);

});
