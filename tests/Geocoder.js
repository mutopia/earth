define([
  'doh/runner',
  '../Geocoder'
], function(doh, Geocoder) {

  doh.register('./Geocoder', [
    {
      name: "locationSuccess",
      setUp: function() {
      },
      runTest: function() {
        var deferred = new doh.Deferred();
        Geocoder.getInstance().deferred.then(function() {
          Geocoder.getInfo('melbourne, australia').then(function(result) {
            setTimeout(deferred.getTestCallback(function() {
              // These may change slightly in the future due to accuracy
              doh.assertTrue(result.success);
              doh.assertEqual(result.name, "Melbourne, Victoria, Australia");
              doh.assertEqual(result.point, "POINT (-37.814107 144.96327999999994)");
              doh.assertEqual(result.coords, {
                lat: -37.814107,
                lng: 144.96327999999994
              });
            }), 100);
          });
        });
        return deferred;
      },
      tearDown: function() {
      },
      timeout: 10000
    },
    {
      name: "locationFailure",
      setUp: function() {
      },
      runTest: function() {
        var deferred = new doh.Deferred();
        Geocoder.getInstance().deferred.then(function() {
          Geocoder.getInfo('my shiny metal ass').then(function(result) {
            setTimeout(deferred.getTestCallback(function() {
              // We hope
              doh.assertFalse(result.success);
            }), 100);
          });
        });
        return deferred;
      },
      tearDown: function() {
      },
      timeout: 10000
    }
  ]);

});
