define([
    'doh/runner',
    '../Extensions',
    './Earth'
], function (doh, Extensions, Earth) {

    var gexx;

    doh.register('./Extensions', [
        Earth[0], // initialises doh.earth
        {
            name: "initExtensions",
            setUp: function () {
                gexx = doh.earth.gexx;
            },
            runTest: function () {
                doh.assertNotEqual(gexx, null);
            }
        },
        function defaults() {
            doh.assertEqual(gexx.polyDefaults2D.lineColor, '#f00');
            doh.assertEqual(gexx.polyDefaults3D.lineColor, '#f00');
        },
        function merge() {
            doh.assertEqual(gexx.merge({b: 2}, {a: 1}), {a: 1, b: 2});
        },
        function mergeDeep() {
            var c = {c: 3};
            var x = {b: c};
            var y = {a: 1};
            var z = {a: 1, b: c};
            doh.assertEqual(gexx.mergeDeep(x, y), z);
            // This does not care about references
            doh.assertEqual(gexx.mergeDeep(x, y).b, c);
            // This does
            doh.assertTrue(gexx.mergeDeep(x, y).b !== c);
        },
        function copy() {
            var x = {a: 1};
            doh.assertEqual(gexx.copy(x), x);
        },
        function options() {
            var x = {
                StatusBarVisibility: false
            };
            gexx.setOptions(x);
            doh.assertEqual(x, gexx.getOptions(['StatusBarVisibility']));
        },
        function closeCoords() {
            doh.assertEqual(gexx.closeCoords([[1,2],[3,4]]), [[1,2],[3,4],[1,2]]);
        },
        function drawPoly3D() {
            doh.assertNotEqual(gexx.drawPoly3D({
                coords: [[1,2],[3,4]]
            }), null);
        },
        function interpolate() {
            doh.assertEqual(gexx.interpolate(5, 0, 10, -20, 20), 0);
            doh.assertEqual(gexx.interpolate(5, 0, 10, 0, 40), 20);
            doh.assertEqual(gexx.interpolate(50, 0, 10, 0, 40, true), 40);
            doh.assertEqual(gexx.interpolate(50, 0, 10, 0, 40, false), 200);
        }
    ]);

});
