define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  './Extensions',
  'utility/Log',
  './lib/google.jsapi',
  // Required
  './lib/extensions-0.2.1.pack'
], function (declare, lang, Extensions, Log, jsapi) {
  return declare(null, {
    // summary:
    //      Instantiates an instance of Google Earth and related modules.

    ge: null, // Google Earth
    gex: null, // Google Earth Extensions
    gexx: null, // Extensions.js instance
    gee: null, // Examples.js instance
    wkte: null, // wkt/Examples.js instance

    constructor: function (elem, callback) {
      // summary:
      //      Loads Google Earth into a DOM element and runs a callback if successful
      this.callback = callback;
      this.init(elem);
    },

    init: function (elem) {
      // summary:
      //      Loads Google Earth into a DOM element
      Log.info("Google Earth starting...");
      jsapi(lang.hitch(this, function () {
        google.load("earth", "1", {callback: lang.hitch(this, function () {
          Log.info("Google Earth loading.");
          google.earth.createInstance(elem, lang.hitch(this, this.initCB), lang.hitch(this, this.failureCB));
        })});
      }));
    },

    initCB: function (object) {
      // summary:
      //      Callback for initialising modules using the returned Google Earth instance
      Log.info("Google Earth has started.");
      this.ge = object;
      this.ge.getWindow().setVisibility(true);
      this.ge.getNavigationControl().setVisibility(this.ge.VISIBILITY_SHOW);
      this.ge.getOptions().setScaleLegendVisibility(this.ge.VISIBILITY_SHOW);
      this.gex = new GEarthExtensions(this.ge);
      this.gexx = Extensions(this.gex);
//      this.gee = new GEExamples(this.gexx);
      if (this.callback) {
        this.callback(this);
      }
    },

    failureCB: function (object) {
      // summary:
      //      Callback for failing to load Google Earth
      throw "Google Earth has failed.";
    }

  });
});
