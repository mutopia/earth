#Google Earth Extensions
This library contains modules which extend the functionality of the [earth-api-utility-library](https://code.google.com/p/earth-api-utility-library/) project.

##Requirements
* Dojo
* [mutopia/utility](https://bitbucket.org/mutopia/utility)

##Usage
Visit `sample/index.html` and view the source.