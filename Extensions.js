define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/_base/array',
  'dojo/_base/Color',
  'dojo/on',
  'dojo/Deferred',
  './wkt/Extensions',
  'utility/Log',
  'utility/Setter',
  // Superclass
  'dojo/Stateful',
  'dojo/Evented',
  // Required
  'utility/Array'
], function (declare, lang, array, Color, on, Deferred, wkt, Log, Setter, Stateful, Evented) {
  return declare([Stateful, Evented], {
    // summary:
    //      Utilities and extensions for earth-api-utility-library
    // description:
    //      See http://code.google.com/p/earth-api-utility-library/

    // TODO investigate if we can use http://code.google.com/p/earth-api-utility-library/wiki/GEarthExtensionsUtilReference#batchExecute(batchFn,_context)

    ge: null,
    gex: null,
    layers: {},

    initArgsName: 'initArgs',
    nextId: 1,
    // Time to wait before callbacks - set to 0 in production
    timedCallbackDelay: 100,

    // TODO may want to extend this to support multiple objects
    // Currently selected object
    selectedObjs: null,
    preSelectedStyleName: 'preSelectedStyle',
    selectedObjStyles: null,
    selectedStyle: {color: '#f00', opacity: 1},
    isSelectDisabled: false,
    isDeselectDisabled: false,
    isSelectionChangeDisabled: false,
    selectCallbacks: null,
    deselectCallbacks: null,

    // Dragging
    dragMode: false,
    dragObject: null,
    dragSensitivity: 0.01,
    dragMinSensitivity: 0,
    dragMaxSensitivity: 0.01,
    dragLatOrigin: null,
    dragLngOrigin: null,
    dragLatLast: null,
    dragLngLast: null,
    dragMouseDownCallback: null,
    dragMouseMoveCallback: null,
    dragMouseUpCallback: null,
    dragViewChange: null,

    _cameraLocked: false,

    // Clicking, stores misc. data
    mouseDown: null,
    mouseUp: null,

    // Zooming
    zoomSensitivity: 1000,
    zoomMinSensitivity: 10,
    zoomMaxSensitivity: 1000,

    // Object currently in edit mode
    editObject: null,
    finishEditCallback: null,

    // Object currently in create mode
    // TODO(aramk) might need to handle many?
    drawObject: null,

    // Camera
    minRange: 0,
    // TODO this should be changed based on the project where needed
    defaultRange: 2000,
    maxRange: 80000,

    // Drawing
    enableRedraw: true,
    // How long to wait in ms before redrawing. If > 0, this will delay redrawing until that amount of time has passed since redraw() was called.
    redrawDelay: 300,
    redrawTimer: null,
    // Minimum number of polygon coordinates (including closing coordinate) to make a triangle
    minCoords: 4,

    // Events
    // TODO maybe replace with a method for checking in JS itself
    domEvents: ['click', 'dblclick', 'mousedown', 'mouseup', 'mousemove', 'mouseover', 'mouseout', 'keydown', 'keyup', 'keypress', 'focus', 'blur', 'change'],
    editEvents: null,

    constructor: function (gex) {
      // summary:
      //      Constructs the module using a GEarthExtensions object
      this.gex = gex;
      this.ge = this.gex.pluginInstance;
      this._initDefaults();
      this._initEvents();
      this.setOptions(this.defaultOptions);
      this.setLayers(this.defaultLayers);
      this.selectedObjs = {};
      this.selectedObjStyles = {};
      this.selectCallbacks = {};
      this.deselectCallbacks = {}
    },

    _initDefaults: function () {
      // summary:
      //      Initialises default settings

      // Disable lines by default.
      this.lineDefaults = {
        lineColor: '#0c0',
        lineWidth: 0,
        lineOpacity: 0
      };

      this.polyDefaults2D = this.merge(this.lineDefaults, {
        coords: [],
        color: '#0c0',
        opacity: 1,
        extrude: false,
        tessellate: true,
        altitudeMode: geo.ALTITUDE_RELATIVE_TO_GROUND
      });

      this.polyDefaults3D = this.merge(this.polyDefaults2D, {
        height: null,
        extrude: true
      });

      this.defaultOptions = {
        StatusBarVisibility: true,
        GridVisibility: false,
        OverviewMapVisibility: false,
        ScaleLegendVisibility: true,
        AtmosphereVisibility: true,
        MouseNavigationEnabled: true,
        UnitsFeetMiles: false
      };

      this.defaultLayers = {
        LAYER_BORDERS: 'Borders',
        LAYER_BUILDINGS: 'Buildings',
        LAYER_BUILDINGS_LOW_RESOLUTION: 'Buildings (Low Res)',
        LAYER_ROADS: 'Roads',
        LAYER_TERRAIN: 'Terrain',
        LAYER_TREES: 'Trees'
      };
    },

    _initEvents: function () {
      // summary:
      //      Initialises default event handlers

      this.dragMouseDownCallback = lang.hitch(this, function (event) {
        var obj = event.getTarget();
        this.lockCamera(true);
//        this.setSelected(obj);
        this.deselectAll();
        this.select(obj);
        this.dragObject = obj;
        this.dragLatOrigin = this.dragLatLast = event.getLatitude();
        this.dragLngOrigin = this.dragLngLast = event.getLongitude();
        if (this.editObject && this.editObject == this.dragObject) {
          // Disable editing while we are dragging so our edit boundary pins update
          this._userStopEdit(this.editObject);
        }
      });

      this.dragMouseMoveCallback = lang.hitch(this, function (event) {
        if (this.dragObject) {
          var lat = event.getLatitude();
          var lng = event.getLongitude();
          var latDiff = lat - this.dragLatLast;
          var lngDiff = lng - this.dragLngLast;
          if (Math.abs(latDiff) > this.dragSensitivity || Math.abs(lngDiff > this.dragSensitivity)) {
            this.addPolyCoords(this.dragObject, [latDiff, lngDiff]);
            this.dragLatLast = lat;
            this.dragLngLast = lng;
            this.redraw();
          }
        }
      });

      this.dragMouseUpCallback = lang.hitch(this, function (event) {
        if (this.editObject && this.editObject == this.dragObject) {
          // Enable editing after drag
          this._userStartEdit(this.editObject);
        }
        if (this.dragObject) {
          Log.info('Stop drag', this.dragObject.getType());
          setTimeout(lang.hitch(this, function () {
            this.lockCamera(false);
            this.deselect(this.dragObject);
          }), 100);
          this._dragEvent(event);
          this.dragObject = this.dragLatOrigin = this.dragLngOrigin = this.dragLatLast = this.dragLngLast = null;
        }
      });

      this.dragViewChange = lang.hitch(this, function (event) {
        this.dragSensitivity = this.getDragSensitivity();
      });

      this.addEvent(this.ge.getGlobe(), 'click', lang.hitch(this, function (event) {
        var validTarget = event.getTarget().getType() == 'GEGlobe';
        if (validTarget && !this.isMousePosChanged(this.mouseDown, this.mouseUp)) {
          this.timedCallback(this.deselectAll);
        }
      }));
    },

    merge: function (orig, changed) {
      // lang.mixin doesn't mixin arrays and deeper objects
      // return lang.mixin(orig, changed);
      return Setter.mixin(orig, changed);
    },

    mergeDeep: function (orig, changed) {
      // Replace with another implementation if needed
      // return lang.mixin(lang.clone(orig), changed);
      return Setter.mixin(lang.clone(orig), changed);
    },

    copy: function (orig) {
      // Replace with another implementation if needed
      return lang.clone(orig);
    },

    // CONFIGURATION

    setOptions: function (options) {
      // summary:
      //      Sets Google Earth options: https://developers.google.com/earth/documentation/reference/interface_g_e_options
      for (option in options) {
        this.setOption(option, options[option]);
      }
    },

    setOption: function (option, value) {
      // summary:
      //      Gets Google Earth option
      this.ge.getOptions()['set' + option](value);
    },

    getOptions: function (options) {
      // summary:
      //      Gets Google Earth options: https://developers.google.com/earth/documentation/reference/interface_g_e_options
      var results = {};
      array.forEach(options, lang.hitch(this, function (option) {
        results[option] = this.getOption(option);
      }));
      return results;
    },

    getOption: function (option) {
      return this.ge.getOptions()['get' + option]();
    },

    lineConfig: function (args) {
      // summary:
      //      Defines the default properties of lines
      return {
        style: {
          line: {
            width: args.lineWidth,
            color: args.lineColor,
            opacity: args.lineOpacity
          }
        }
      }
    },

    polyConfig2D: function (args) {
      // summary:
      //      Defines the default properties of 2D polygons, building on that of lines
      return this.mergeDeep(this.lineConfig(args), {
        style: {
          poly: {
            color: args.color,
            opacity: args.opacity
          }
        }
      });
    },

    polyConfig3D: function (args) {
      // summary:
      //      Defines the default properties of 3D polygons, building on that of 2D polygons
      return this.mergeDeep(this.polyConfig2D(args), {
        polygon: {
          polygon: args.coords,
          extrude: args.extrude,
          tessellate: args.tessellate,
          altitudeMode: args.altitudeMode
        }
      });
    },

    // TODO(aramk) This is a work in progress which I stopped short - the code may be useful if it's needed in the future.
//    polyToArgs: function (poly) {
//      // summary:
//      //    Returns the arguments which would create the given polygon.
//      return {
//        height: this.getHeight(poly),
//        altitude: this.getAltitude,
//        style: {
//          color: args.color,
//          borderColor: args.lineColor,
//          opacity: args.opacity,
//          borderOpacity: args.lineOpacity
//        },
//        footprint: {
//          polygon: args.coords ? wkt.coordsToWKT(this.getOuterBoundariesCoords(poly)) : null
//        }
//      };
//    },

    // MODIFYING POINTS

    setCoordsAltitude: function (/*Number[][]*/ coords, /*Number*/ height) {
      // summary:
      //      Sets or replaces an altitude to a set of coordinates.
      if (!coords || !coords.length || !coords[0].length) {
        Log.warn('setAltitude needs valid coordinates, aborting', coords);
        return;
      }
      if (coords[0][0] instanceof Array) {
        // Array of arrays of arrays
        var result = [];
        for (var i = 0; i < coords.length; i++) {
          // TODO refactor this
          result[i] = this.setCoordsAltitude(coords[i], height);
        }
        return result;
      } else {
        // Array of arrays
        coords = this.copy(coords);
        coords.map(lang.hitch(this, function (coord) {
          coord[2] = height;
        }));
        return coords;
      }
    },

    addCoordsAltitude: function (/*Number[][]*/ coords, /*Number*/ height) {

      if (coords[0][0] instanceof Array) {
        // Array of arrays of arrays
        var result = [];
        for (var i = 0; i < coords.length; i++) {
          result[i] = this.addCoordsAltitude(coords[i], height);
        }
        return result;
      } else {
        // Array of arrays
        coords = this.copy(coords);
        coords.map(lang.hitch(this, function (coord) {
          coord[2] = (coord[2] || 0) + height;
        }));
        return coords;
      }
    },

    modifyCoordsAltitude: function (/*Number[][]*/ coords, /*Function*/ callback) {
      // summary:
      //      Sets or replaces an altitude to a set of coordinates.
      coords = this.copy(coords);
      for (var i = 0; i < coords.length; i++) {
        var coord = coords[i];
        if (coord.length == 2) {
          coord.push(height);
        } else if (coord.length == 3) {
          coord[2] = height;
        }
      }
      return coords;
    },

    closeCoords: function (/*Number[][]*/ coords) {
      // summary:
      //      Closes an open polygon.
      // description:
      //      Usually not required since polygons are closed when drawing.
      if (typeof coords == 'undefined') {
        return coords;
      } else {
        if (!coords.first().equals(coords.last())) {
          coords.push(coords.first());
        }
        return coords;
      }
    },

    // BUILDING

    // TODO abstract other building functions

    getNextId: function () {
      return (this.nextId++).toString();
    },

    buildPolygonPlacemark: function (coords, args) {
      coords = this.copy(coords);
      var placemark = null;
      if (coords && coords.length && coords[0].length) {
        // [x,...]
        var hasLinearRings = coords[0][0] instanceof Array;
        if (!hasLinearRings) {
          // Currently [[lat,lng,alt], ...]
          // Only outer ring, no inner
          coords = [coords];
        }
        // Now [[[lat,lng,alt], ...],...]
      } else {
        if (args.style) {
          // To apply the style, we pass to the util library
          args.id = this.getNextId();
          if (!args.polygon) Log.warn('Attempting to render object with no polygon:', args);
          placemark = this.gex.dom.buildPlacemark(args);
        } else {
          placemark = this.ge.createPlacemark(this.getNextId());
        }
        return placemark;
      }

      // TODO use http://dev.openlayers.org/apidocs/files/OpenLayers/Format/KML-js.html

      // TODO reuse this styling for the prism
      var polyColor = this.createABGRColor(args.style.poly.color, args.style.poly.opacity);
      var lineColor = this.createABGRColor(args.style.line.color, args.style.line.opacity);

      var isFlatPoly = coords && coords.length && coords[0].length && (coords[0][0].length < 3 || coords[0][0][2] === 0);

      var kmlString = '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\
            <Placemark id="' + this.getNextId() + '">\
              <Style>\
                <LineStyle>\
                  <width>' + args.style.line.width + '</width>\
                  <color>' + lineColor + '</color>\
                </LineStyle>\
                <PolyStyle>\
                  <color>' + polyColor + '</color>\
                </PolyStyle>\
              </Style>\
              <Polygon>'
      if (!isFlatPoly) {
        kmlString += '<extrude>' + (args.polygon && args.polygon.extrude ? 1 : 0) + '</extrude>\
        <tessellate>' + (args.polygon && args.polygon.tessellate ? 1 : 0) + '</tessellate>\
        <altitudeMode>relativeToGround</altitudeMode>';
      }
      kmlString += '<outerBoundaryIs>' + this.buildKmlLinearRing(coords[0]) + '</outerBoundaryIs>\
                <innerBoundaryIs>';
      for (var i = 1; i < coords.length; i++) {
        kmlString += this.buildKmlLinearRing(coords[i]);
      }
      kmlString +=
          '</innerBoundaryIs>\
          </Polygon>\
        </Placemark>\
      </kml>';
      var placemark = this.ge.parseKml(kmlString);
      return placemark;
    },

    _buildPolygonPlacemark_deprecated: function (coords, args) {
      // summary:
      //    Deprecated version of buildPolygonPlacemark()
      if (coords && coords.length && coords[0].length) {
        var hasLinearRings = coords[0][0] instanceof Array;
        if (hasLinearRings) {
          var polygon = this.ge.createPolygon('');
          for (var i = 0; i < coords.length; i++) {
            var coord = coords[i];
            // Coordinates represent a polygon with inner rings
            var ring = this.gex.dom.buildLinearRing(coord);
            if (i == 0) {
              polygon.setOuterBoundary(ring);
            } else {
              // If we have a single array in coords, this is ignored (simple polygon)
              polygon.getInnerBoundaries().appendChild(ring);
            }
          }
          if (args.polygon) {
            // For 3D polygons
            polygon.setExtrude(args.polygon.extrude);
            polygon.setTessellate(args.polygon.tessellate);
            polygon.setAltitudeMode(args.polygon.altitudeMode);
          }
          args.polygon = polygon;
        }
      } else {
        args.polygon = this.gex.dom.buildPolygon([]);
      }
      // Warning: ID is not set for this
      return this.gex.dom.buildPlacemark(args);
    },

    buildKmlLinearRing: function (coords) {
      return '<LinearRing>\
              <coordinates>\
              ' + this.buildKmlRingCoords(coords) + '\
                      </coordinates>\
                    </LinearRing>';
    },

    buildKmlLinearRings: function (coords) {
      var kml = '';
      for (var i = 0; i < coords.length; i++) {
        kml += this.buildKmlLinearRing(coords[i]);
      }
      return kml;
    },

    buildKmlRingCoords: function (coords) {
      coords = this.copy(coords);
      var coordsStr = '';
      for (var i = 0; i < coords.length; i++) {
        var coord = coords[i];
        // TODO factor this somewhere else?
        var lat = coord[0];
        var lng = coord[1];
        coord[0] = lng;
        coord[1] = lat;
        coordsStr += ' ' + coord.join(',');
      }
      return coordsStr;
    },

    buildKmlPolygon: function (coords, args) {
      return '<Polygon>\
            <extrude>' + (args.polygon.extrude ? 1 : 0) + '</extrude>\
            <tessellate>' + (args.polygon.tessellate ? 1 : 0) + '</tessellate>\
            <altitudeMode>relativeToGround</altitudeMode>\
            <outerBoundaryIs>'
          + this.buildKmlLinearRing(coords) +
          '</outerBoundaryIs>\
        </Polygon>';
    },

    buildPoly3D: function (args) {
      // summary:
      //      Builds 3D polygons using the given arguments.
      // description:
      //      Can specify "height" (Number) argument as well as those from drawPoly2D().
      args = this.mergeDeep(this.polyDefaults3D, args);
      if (args.coords && args.coords.length && args.height != null) {
        args.coords = this.setCoordsAltitude(args.coords, args.height);
      }
      var coords = args.coords;
      args = this.polyConfig3D(args);
      var argsCopy = this.copy(args);
      return this.buildPolygonPlacemark(coords, argsCopy);
    },

    _buildPoly3D_deprecated: function (args) {
      // summary:
      //    Deprecated version of buildPoly3D()
      args = this.mergeDeep(this.polyDefaults3D, args);
      if (args.coords && args.coords.length && args.height != null) {
        args.coords = this.setCoordsAltitude(args.coords, args.height);
      }
      var coords = args.coords;
      args = this.polyConfig3D(args);
      var argsCopy = this.copy(args);
      var placemark = this._buildPolygonPlacemark_deprecated(coords, argsCopy);
      return placemark;
    },

    buildPrism: function (args) {
      args = this.merge({
        extrude: false
      }, args);
      args = this.mergeDeep(this.polyDefaults3D, args);
      args.extrude = false;
      var coords = args.coords;

      // TODO this code is copied from buildKmlPlacemark - can we refactor?
      if (coords && coords.length && coords[0].length) {
        // [x,...]
        var hasLinearRings = coords[0][0] instanceof Array;
        if (!hasLinearRings) {
          // Currently [[lat,lng,alt], ...]
          // Only outer ring, no inner
          coords = [coords];
        }
        // Now [[[lat,lng,alt], ...],...]
      } else {
        // TODO this is duplicated, refactor
        return this.ge.createPlacemark(this.getNextId());
      }
      if (args.altitude) {
        coords = this.addCoordsAltitude(coords, args.altitude);
      }
      var height = args.height;
      delete args.height;

      // TODO handle case when we have inner rings
      var bottomCoords = this.copy(coords[0]);
      args = this.polyConfig3D(args);
      var topCoords = this.addCoordsAltitude(bottomCoords, height);

      // TODO can't make the shape with a single polygon... http://zophusx.byethost11.com/tutorial.php?lan=dx9&num=11

      var polyColor = this.createABGRColor(args.style.poly.color, args.style.poly.opacity);
      var lineColor = this.createABGRColor(args.style.line.color, args.style.line.opacity);

      var prismStr = '<kml xmlns="http://www.opengis.net/kml/2.2" xmlns:gx="http://www.google.com/kml/ext/2.2" xmlns:kml="http://www.opengis.net/kml/2.2" xmlns:atom="http://www.w3.org/2005/Atom">\
          <Placemark id="' + this.getNextId() + '">\
            <Style>\
              <LineStyle>\
                <width>' + args.style.line.width + '</width>\
                <color>' + lineColor + '</color>\
              </LineStyle>\
              <PolyStyle>\
                <color>' + polyColor + '</color>\
              </PolyStyle>\
            </Style>\
	        <MultiGeometry>';

      for (var i = 0; i + 1 < bottomCoords.length; i++) {
        var face = [topCoords[i], topCoords[i + 1], bottomCoords[i + 1], bottomCoords[i], topCoords[i]];
        prismStr += this.buildKmlPolygon(face, args);
      }

      prismStr += this.buildKmlPolygon(bottomCoords, args);
      prismStr += this.buildKmlPolygon(topCoords, args);
      prismStr += '</MultiGeometry>\
          </Placemark>\
          </kml>';

      return this.ge.parseKml(prismStr);
    },

    clone: function (kmlFeature) {
      // summary:
      //    Returns a clone of the given KmlFeature.
      return this.ge.parseKml(kmlFeature.getKml())
    },

    // DRAWING

    drawPoly3D: function (args) {
      // summary:
      //      Draws 3D polygons using the given arguments.
      // description:
      //      Can specify "height" (Number) argument as well as those from drawPoly2D().
      var poly = this.buildPoly3D(args);
      this.append(poly);
      return poly;
    },

    drawPrism: function (args) {
      var prism = this.buildPrism(args);
      this.append(prism);
      return prism;
    },

    drawPoly2D: function (args) {
      // summary:
      //      Draws 2D polygons using the given arguments.
      // description:
      //      Can specify "coords" (Number[][][]), "color" (String) and "opacity" (Number 0-1) arguments as well as those from drawLine().
      args = this.mergeDeep(this.polyDefaults2D, args);
      var poly = this.buildPolygonPlacemark(args.coords, this.polyConfig2D(args));
      this.append(poly);
      return poly;
    },

    drawLine: function (args) {
      // summary:
      //      Draws lines using the given arguments.
      // description:
      //      Can specify "lineColor" (String), "lineWidth" (Number) and "lineOpacity" (Number 0-1) arguments.
      args = this.mergeDeep(this.lineDefaults, args);
      return this.gex.dom.addLineStringPlacemark(args.coords, this.lineConfig(args));
    },

    drawPoint: function (args) {
      // summary:
      //      Draws points using the given arguments.
      // description:
      //      Can specify "name" (String) and "point" (Number[]) arguments.
      return this.gex.dom.addPlacemark({
            name: args.name,
            point: new geo.Point(args.point)
          }
      );
    },

    setPolyStyle: function (poly, args) {
      // summary:
      //      Modifies the existing polygon style for a KmlObject.
      // description:
      //      The "color" argument is an RGB hex string, array or object.
      //      The "opacity" argument is a Number from 0 - 1.
      if (args.color) {
        this.setColorRGB(poly, args.color);
      }
      if (args.opacity) {
        this.setOpacity(poly, args.opacity);
      }
    },

    getPolyStyle: function (poly) {
      // summary:
      //      Returns the existing polygon style for a KmlObject.
      // description:
      //      The "color" argument is an RGB hex string, array or object.
      //      The "opacity" argument is a Number from 0 - 1.
      var color = this.getColor(poly);
      var rgb = this.getColorRGB(color);
      var opacity = this.getOpacity(poly);
      return {color: rgb, opacity: opacity};
    },

    getColor: function (poly) {
      // summary:
      //      Returns the Kml color object for a polygon.
      return poly.getComputedStyle().getPolyStyle().getColor();
    },

    setColor: function (poly, color) {
      // summary:
      //      Sets the KmlColor of the polygon.
      var rgb = this.getColorRGB(color);
      this.setColorRGB(poly, rgb);
    },

    setColorRGB: function (poly, rgb) {
      // summary:
      //      Sets the rgb color for a polygon.
      var color = this.getColor(poly);
      this._setColorRGB(color, rgb);
    },

    getColorRGB: function (color) {
      // summary:
      //      Returns the color of the polygon as an array of RGB values.
      return [color.getR(), color.getG(), color.getB()];
    },

    _setColorRGB: function (color, rgb) {
      // summary:
      //      Sets the given KmlColor to the rgb.
      if (color) {
        if (typeof rgb == 'string') {
          rgb = (new Color(rgb)).toRgb();
        } else if (rgb.constructor == Object) {
          rgb = [rgb.r, rgb.g, rgb.b];
        }
        color.setR(rgb[0]);
        color.setG(rgb[1]);
        color.setB(rgb[2]);
      }
      this.redraw();
    },

    setOpacity: function (poly, opacity) {
      // summary:
      //      Sets the opacity (0-1) of a polygon.
      var color = this.getColor(poly);
      this._setOpacity(color, opacity);
    },

    _setOpacity: function (color, opacity) {
      // summary:
      //      Sets the opacity (0-1) of the given KmlColor.
      if (color) {
        color.setA(opacity * 255);
      }
      this.redraw();
    },

    getOpacity: function (poly) {
      // summary:
      //      Returns the opacity (0-1) of a polygon.
      var color = this.getColor(poly);
      return this._getOpacity(color);
    },

    _getOpacity: function (color) {
      // summary:
      //      Sets the opacity (0-1) of a KmlColor.
      if (color) {
        return color.getA() / 255;
      } else {
        return null;
      }
    },

    setLineStyle: function (poly, args) {
      // summary:
      //      Modifies the existing line style for a KmlObject.
      // description:
      //      The "color" argument is an RGB Hex String.
      //      The "opacity" argument is a Number from 0 - 1.

      if (args.color) {
        this.setLineColorRGB(poly, args.color);
      }
      if (args.opacity) {
        this.setLineOpacity(poly, args.opacity);
      }
    },

    getLineColor: function (poly) {
      // summary:
      //      Returns the KmlColor object for a line or polygon outline.
      return poly.getComputedStyle().getLineStyle().getColor();
    },

    setLineColor: function (poly, color) {
      // summary:
      //      Sets the KmlColor of a line or polygon outline.
      var rgb = this.getColorRGB(color);
      this.setLineColorRGB(poly, rgb);
    },

    setLineColorRGB: function (poly, rgb) {
      // summary:
      //      Sets the rgb color for a line or polygon out.
      var color = this.getLineColor(poly);
      this._setColorRGB(color, rgb);
    },

    setLineOpacity: function (poly, opacity) {
      // summary:
      //      Sets the opacity of a line or polygon outline.
      var color = this.getLineColor(poly);
      this._setOpacity(color, opacity);
    },

    getLineOpacity: function (poly) {
      // summary:
      //      Gets the opacity of a line or polygon outline.
      var color = this.getLineColor(poly);
      return this._getOpacity(color);
    },

    createABGRColor: function (color, opacity) {
      var a = new Color(color).toRgb();
      var b = new Color([a[0], a[1], a[2], opacity]);
      // KML uses "aabbggrr"
      var toHexString = function (num) {
        var hex = num.toString(16);
        return hex.length == 1 ? '0' + hex : hex;
      };
      return toHexString(Math.floor(b.a * 255)) + toHexString(b.b) + toHexString(b.g) + toHexString(b.r);
    },

    redraw: function (ge) {
      // summary:
      //      Refreshes the Google Earth display.
      // description:
      //      Some modifications to objects on the globe do not take effect until the view changes or
      //      mouse events are captured. This aims to force a redraw.
      if (this.enableRedraw) {
        ge = Setter.def(ge, this.ge);
        if (ge) {
          if (this.redrawTimer) {
            clearTimeout(this.redrawTimer);
            this.redrawTimer = null;
          }
          if (this.redrawDelay <= 0) {
            this._redraw(ge);
          } else {
            this.redrawTimer = setTimeout(lang.hitch(this, function () {
              this._redraw(ge);
              this.redrawTimer = null;
            }), this.redrawDelay);
          }

        } else {
          Log.error('Earth redraw failed');
        }
      }
    },

    _redraw: function (ge) {
      var locked = this._cameraLocked;
      // TODO(aramk) could cause race condition if another thread tried to change lock in between
      this.lockCamera(!locked);
      this.lockCamera(locked);
      Log.debug('Earth redraw');
    },

    _setEnableRedrawAttr: function (value) {
      this.enableRedraw = value;
      Log.info('Earth enable redraw', value);
    },

    // ZOOMING

    setLookAt: function (lat, lng, range) {
      // summary:
      //      Sets camera position based on location and range.
      range = Setter.def(range, this.defaultRange);
      var lookAt = this.getLookAt();
      lookAt.setLatitude(lat);
      lookAt.setLongitude(lng);
      lookAt.setRange(range);
      this.ge.getView().setAbstractView(lookAt);
    },

    setZoom: function (range) {
      // summary:
      //      Sets camera range.
      var lookAt = this.getLookAt();
      lookAt.setRange(range);
      this.ge.getView().setAbstractView(lookAt);
    },

    getZoom: function () {
      // summary:
      //      Gets camera range.
      var lookAt = this.getLookAt();
      return lookAt.getRange();
    },

    zoomIn: function () {
      // summary:
      //      Zooms camera in.
      this._zoomChange(-1);
    },

    zoomOut: function () {
      // summary:
      //      Zooms camera out.
      this._zoomChange(1);
    },

    _zoomChange: function (sign) {
      // summary:
      //      Underlying zoom method.
      // sign: int
      //      Multiplier which changes the zoom level to the current + sign * sensitivity
      // description:
      //      Defines the zoom sensitivity based on current zoom. Sets the new zoom based on a given sign. Ensures zoom is >= 0;
      this.zoomSensitivity = this.getZoomSensitivity();
      var zoom = this.getZoom();
      var newZoom = zoom + sign * this.zoomSensitivity;
      if (newZoom > 0 || (zoom > 0 && newZoom == 0)) {
        this.setZoom(newZoom);
      }
    },

    getZoomSensitivity: function () {
      // summary:
      //      Gets zoom sensitivity based on the current range (zoom level).
      return this.interpolate(this.getZoom(), this.minRange, this.defaultRange, this.zoomMinSensitivity, this.zoomMaxSensitivity, true);
    },

    // DRAGGING

    addDragEvent: function (obj) {
      // summary:
      //      Enables drag mode for an object.
      // description:
      //      This must be called for each draggable object along with setDragMode(true).
      this.addEvent(obj, 'mousedown', this.dragMouseDownCallback);
    },

    removeDragEvent: function (obj) {
      // summary:
      //      Disables drag mode for an object. This must be called for each draggable object along with setDragMode(false).
      this.removeEvent(obj, 'mousedown', this.dragMouseDownCallback);
    },

    setDragMode: function (mode) {
      // summary:
      //      Sets dragging mode on and off
      if (mode == this.dragMode) {
        Log.info('Drag mode is already', mode);
      } else {
        this.dragMode = mode;
        Log.info('Drag mode set', mode);
        if (mode) {
          this.isSelectDisabled = true;
          this.isDeselectDisabled = true;
          this.addEvent(this.ge.getGlobe(), 'mousemove', this.dragMouseMoveCallback);
          this.addEvent(this.ge.getGlobe(), 'mouseup', this.dragMouseUpCallback);
          this.addEvent(this.ge.getView(), 'viewchange', this.dragViewChange, false);
        } else {
          this.isSelectDisabled = false;
          this.isDeselectDisabled = false;
          this.removeEvent(this.ge.getGlobe(), 'mousemove', this.dragMouseMoveCallback);
          this.removeEvent(this.ge.getGlobe(), 'mouseup', this.dragMouseUpCallback);
          this.removeEvent(this.ge.getView(), 'viewchange', this.dragViewChange, false);
        }
      }
    },

    getDragSensitivity: function () {
      // summary:
      //      Sets dragging sensitivity based on current camera range (zoom level)
      return this.interpolate(this.getZoom(), this.minRange, this.maxRange, this.dragMinSensitivity, this.dragMaxSensitivity, true);
    },

    _dragEvent: function (event) {
      // summary:
      //      Helper function for moving drag object
      var latDiff = event.getLatitude() - this.dragLatLast;
      var lngDiff = event.getLongitude() - this.dragLngLast;
      if (!(latDiff == 0 && lngDiff == 0)) {
        this.addPolyCoords(this.dragObject, [latDiff, lngDiff]);
        Log.info('Moved ' + latDiff + ', ' + lngDiff);
      }
    },

    // TILTING

    setTilt: function (tilt) {
      // summary:
      //      Sets the tilt angle of the camera.
      // description:
      //      90 is tangential to the earth, 0 is looking towards the ground.
      var lookAt = this.getLookAt();
      lookAt.setTilt(tilt);
      this.ge.getView().setAbstractView(lookAt);
    },

    getTilt: function () {
      // summary:
      //      Gets the tilt angle of the camera.
      var lookAt = this.getLookAt();
      return lookAt.getTilt();
    },

    // CAMERA MISC

    getLookAt: function () {
      // summary:
      //      Gets the LookAt object from the Google Earth View object
      return this.ge.getView().copyAsLookAt(this.ge.ALTITUDE_RELATIVE_TO_GROUND);
    },

    getCameraMetrics: function () {
      // summary:
      //      Converts the LookAt and Camera objects from the Google Earth
      //      View object to an object that can be used externally to Google Earth.
      var cameraMetrics = {};
      var lookAt = this.getLookAt();
      var geCamera = this.ge.getView().copyAsCamera(this.ge.ALTITUDE_RELATIVE_TO_GROUND);

      cameraMetrics.tilt = geCamera.getTilt();
      cameraMetrics.roll = geCamera.getRoll();
      cameraMetrics.heading = geCamera.getHeading();
      cameraMetrics.altitude = geCamera.getAltitude();
      cameraMetrics.latitude = geCamera.getLatitude();
      cameraMetrics.longitude = geCamera.getLongitude();
      cameraMetrics.lookAltitude = lookAt.getAltitude();
      cameraMetrics.lookLatitude = lookAt.getLatitude();
      cameraMetrics.lookLongitude = lookAt.getLongitude();

      return cameraMetrics;
    },

    setCameraMetrics: function (cameraMetrics) {
      Log.debug('Setting Earth camera to ', cameraMetrics);
      var camera = this.ge.getView().copyAsCamera(this.ge.ALTITUDE_RELATIVE_TO_GROUND);
      var lookAt = this.ge.getView().copyAsLookAt(this.ge.ALTITUDE_RELATIVE_TO_GROUND);
      Log.debug('Previous camera is ', camera);

      camera.setLatitude(cameraMetrics.latitude);
      camera.setLongitude(cameraMetrics.longitude);
      camera.setAltitude(cameraMetrics.altitude);
      camera.setTilt(cameraMetrics.tilt);
      camera.setHeading(cameraMetrics.heading);
      camera.setAltitude(cameraMetrics.altitude);

      this.ge.getView().setAbstractView(camera);
    },

    flyTo: function (obj, args) {
      // summary:
      //      Moves the camera towards the given KmlObject
      args = Setter.def(args, undefined);
      this.gex.util.flyToObject(obj, args);
    },

    lockCamera: function (lock) {
      // summary:
      //      Convenience method for this.gex.util.takeOverCamera
      // lock: Boolean
      //      Locks the camera if true.
      this._cameraLocked = lock;
      this.gex.util.takeOverCamera(lock);
    },

    // EVENTS

    addEvent: function (targetObject, eventID, listenerCallback, capture) {
      // summary:
      //      Convenience method for google.earth.addEventListener
      capture = Setter.def(capture, true);
      google.earth.addEventListener(targetObject, eventID, listenerCallback, capture);
    },

    removeEvent: function (targetObject, eventID, listenerCallback, capture) {
      // summary:
      //      Convenience method for google.earth.removeEventListener
      capture = Setter.def(capture, true);
      google.earth.removeEventListener(targetObject, eventID, listenerCallback, capture);
    },

    addEvents: function (targetObject, eventIDs, capture) {
      // summary:
      //      Calls addEvent with every item in the eventIDs object.
      // eventIDs: Object
      //      The keys should be eventID strings and the values should be the listener callbacks.
      for (var eventID in eventIDs) {
        var listenerCallback = eventIDs[eventID]
        this.addEvent(targetObject, eventID, listenerCallback, capture);
      }
    },

    removeEvents: function (targetObject, eventIDs, capture) {
      // summary:
      //      Calls removeEvent with every item in the eventIDs object.
      // eventIDs: Object
      //      The keys should be eventID strings and the values should be the listener callbacks.
      for (var eventID in eventIDs) {
        var listenerCallback = eventIDs[eventID]
        this.removeEvent(targetObject, eventID, listenerCallback, capture);
      }
    },

    // TODO apply this to all callbacks! Beware of any that may result in poor performance
    // TODO make a variable to toggle whether a timeout is used or if the callback executes immediately
    timedCallback: function (callback, args, delay) {
      // summary:
      //    Runs the given callback function after a delay to prevent exceptions from being consumed by Google Earth
      if (callback) {
        setTimeout(lang.hitch(this, function () {
          callback.apply(this, args);
        }), delay || this.timedCallbackDelay);
      }
    },

    // PROPERTIES

    setInitArgs: function (obj, /*Object*/ value) {
      // summary:
      //      Sets initial properties for a KmlObject
      return this.setData(obj, this.initArgsName, value);
    },

    getInitArgs: function (obj) {
      // summary:
      //      Gets initial properties for a KmlObject
      return this.getData(obj, this.initArgsName);
    },

    setData: function (obj, /*String*/ key, /*Object*/ value) {
      // summary:
      //      Sets JS data for a KmlObject

      // TODO this increases linearly with each render - find a better way
      //return this.gex.util.setJsDataValue(obj, key, value);
    },

    getData: function (obj, /*String*/ key) {
      // summary:
      //      Gets JS data for a KmlObject

      // TODO this increases linearly with each render - find a better way
//      return this.gex.util.getJsDataValue(obj, key);
    },

    // MISC

    interpolate: function (x, x1, x2, y1, y2, restrict) {
      // summary:
      //      Performs linear interpolation.
      // restrict: Boolean
      //      Constrains the x value in the given bounds.
      // description:
      //      Takes a set of Numbers x1 and x2 on the x-axis and y1 and y2 on the y-axis and returns the y value
      //      given an x value.

      // TODO move this elsewhere, not limited to earth
      if (restrict) {
        if (x < x1) {
          x = x1;
        } else if (x > x2) {
          x = x2;
        }
      }
      var m = (y2 - y1) / (x2 - x1);
      return m * x + (y1 - m * x1);
    },

    // SELECTION

    getSelected: function () {
      // summary:
      //      Gets the selected object.
      return this.selectedObjs;
    },

    _getId: function (obj) {
      var id = obj.getId();
      if (typeof id != 'undefined' && id !== null) {
        return id;
      } else {
        return null;
      }
    },

    select: function (obj) {
      if (this.isSelectDisabled) {
        Log.warn('Select is disabled');
        return;
      }
      var id = this._getId(obj);
      if (id !== null) {
        if (this.selectedObjs[id] !== obj) {
//          if (typeof this.selectedObjs[id] != 'undefined') {
//            // Prevents a second select change event
//            this.isSelectionChangeDisabled = true;
////            this.deselect(obj);
//            this.isSelectionChangeDisabled = false;
//          }
          this.selectedObjs[id] = obj;
          this._select(obj);
        } else {
          Log.warn('Object already selected', id);
        }
      } else {
        Log.warn('Could not select object without ID', obj, id, this.selectedObjs[id]);
      }
    },

    _select: function (obj) {
      var id = this._getId(obj);
      this.selectedObjStyles[id] = this.getPolyStyle(obj);
      this.setPolyStyle(obj, this.selectedStyle);
      this._onSelectionChange();
      var callbacks = this.selectCallbacks[id];
      array.forEach(callbacks, lang.hitch(this, function (callback) {
        callback(obj);
      }));
    },

    deselect: function (obj) {
      if (this.isDeselectDisabled) {
        Log.warn('Deselect is disabled');
        return;
      }
      var id = this._getId(obj);
      if (id !== null) {
        if (this.selectedObjs[id] === obj) {
          this._deselect(obj);
        }
      } else {
        Log.warn('Could not deselect object without ID', obj, id);
      }
    },

    _deselect: function (obj) {
      var id = this._getId(obj);
      this.setPolyStyle(obj, this.selectedObjStyles[id]);
      this._onSelectionChange();
      delete this.selectedObjStyles[id];
      delete this.selectedObjs[id];
      var callbacks = this.deselectCallbacks[id];
      array.forEach(callbacks, lang.hitch(this, function (callback) {
        callback(obj);
      }));
    },

    deselectAll: function () {
      var ids = [];
      for (var id in this.selectedObjs) {
        ids.push(id);
      }
      array.forEach(ids, lang.hitch(this, function (id) {
        var obj = this.selectedObjs[id];
        this.deselect(obj);
      }));
    },

    isSelected: function (obj) {
      var id = this._getId(obj);
      return typeof this.selectedObjs[id] != 'undefined';
    },

    _onSelectionChange: function () {
      if (!this.isSelectionChangeDisabled) {
        on.emit(this, 'selection-change', {
          bubbles: true,
          cancelable: true
        });
      }
    },

    // TODO move elsewhere
    drawArea: function (callback) {
      this.userDrawPoly({
        finishCallback: lang.hitch(this, function (poly, coords) {
          this.timedCallback(callback, [poly, coords]);
        }),
        polygon: {
          coords: [[]],
          color: '#93d0ff',
          lineColor: '#0090ff',
          opacity: 0.8,
          lineOpacity: 1
        }
      });
    },

    selectArea: function (callback) {
      this.isSelectDisabled = true;
      this.drawArea(lang.hitch(this, function (poly, coords) {
        this.isSelectDisabled = false;
        this.deselectAll();
        // Prevents clicks on the globe from deselecting
        this.isDeselectDisabled = true;
        var areaWkt = wkt.coordsToWKTObject(coords);
        var children = this.getVisiblePlacemarkChildren();
        for (var i = 0; i < children.length; i++) {
          var childPoly = children[i];
          if (childPoly.getVisibility()) {
            var childCoords = this.getOuterBoundary(childPoly, 2);
            if (childCoords && childCoords.length) {
              var childWkt = wkt.coordsToWKTObject(childCoords);
              if (areaWkt.intersects(childWkt)) {
                this.select(childPoly);
              }
            }
          }
        }
        this.remove(poly);
        if (callback) {
          this.timedCallback(callback, arguments);
        }
        // Prevents globe double click from deselecting immediately after selection
        setTimeout(lang.hitch(this, function () {
          this.isDeselectDisabled = false;
        }), 1000);
      }));
    },

    addSelectCallback: function (obj, callback) {
      var id = this._getId(obj);
      if (id) {
        this.selectCallbacks[id] = this.selectCallbacks[id] || [];
        this.selectCallbacks[id].push(callback);
      }
    },

    addDeselectCallback: function (obj, callback) {
      var id = this._getId(obj);
      if (id) {
        this.deselectCallbacks[id] = this.deselectCallbacks[id] || [];
        this.deselectCallbacks[id].push(callback);
      }
    },

    makeSelectable: function (obj, args) {
      // TODO docs
      args = lang.mixin({
        event: 'click',
        selectCallback: null,
        deselectCallback: null
      }, args);
      if (args.selectCallback) {
        this.addSelectCallback(obj, args.selectCallback);
      }
      if (args.deselectCallback) {
        this.addDeselectCallback(obj, args.deselectCallback);
      }
      this.addEvent(obj, args.event, lang.hitch(this, function (event) {
        this.deselectAll();
        this.select(event.getTarget());
      }));

      // TODO prevent deselect if dragging the map?
      // TODO store and remove the event when we destroy the polygon

      // TODO we should only need one of these

//      this.addEvent(this.ge.getGlobe(), args.event, lang.hitch(this, function (event) {
//        var validTarget = event.getTarget().getType() == 'GEGlobe';
//        if (validTarget) {
//          var oldSelected = this.getSelected();
//          if (oldSelected == obj) {
//            this.deselectAll();
//            if (args.deselectCallback) {
//              args.deselectCallback(event);
//            }
////            this.setSelected(null, function () {
////              if (args.deselectCallback) {
////                args.deselectCallback(event);
////              }
////            });
//          }
//        }
//      }));
    },

    removeSelectable: function () {
      // TODO call when destorying the poly? is this done automatically by Earth?
    },

    // BOUNDARIES

    _getBoundary: function (type, polygon, dimensions) {
      // summary:
      //      Returns a Number[][] of the coordinates to a given KmlPolygon.
      // description:
      //      Avoid using this, since coords need to be transformed. Only use it if you need an array of the coordinates.
      // type: String
      //      Must be "Outer" or "Inner" for respective boundary.

      var coords = this['get' + type + 'BoundaryCoords'](polygon);
      var coordArray = [];
      dimensions = Setter.def(dimensions, 3);
      if (coords) {
        for (var i = 0; i < coords.getLength(); i++) {
          var coord = coords.get(i);
          coordArray[i] = [];
          if (dimensions > 0) {
            coordArray[i].push(coord.getLatitude());
          }
          if (dimensions > 1) {
            coordArray[i].push(coord.getLongitude());
          }
          if (dimensions > 2) {
            coordArray[i].push(coord.getAltitude());
          }
        }
      }
      return coordArray;
    },

    getOuterBoundary: function (polygon, dimensions) {
      // summary:
      //      Returns the outer boundary of a KmlPolygon as an array of coordinates.
      return this._getBoundary('Outer', polygon, dimensions);
    },

    getInnerBoundary: function (polygon, dimensions) {
      // summary:
      //      Returns the inner boundary of a KmlPolygon as an array of coordinates.
      return this._getBoundary('Inner', polygon, dimensions);
    },

    setOuterBoundary: function (polygon, coords) {
      // summary:
      //      Sets the outer boundary of a KmlPolygon using an array of coordinates.
      this._setBoundary('Outer', polygon, coords);
    },

    setInnerBoundary: function (polygon, coords) {
      // summary:
      //      Sets the inner boundary of a KmlPolygon using an array of coordinates.
      this._setBoundary('Inner', polygon, coords);
    },

    _setBoundary: function (type, polygon, coords) {
      // summary:
      //      Sets the boundary of a KmlPolygon as an array of coordinates.
      // type: String
      //      Either "Inner" or "Outer".
      var poly = new geo.Polygon(coords);
      var ring = this.gex.dom.buildLinearRing(poly[type.toLowerCase() + 'Boundary']());
      polygon.getGeometry()['set' + type + 'Boundary'](ring);
    },

    displaceCoords: function (coords, displ, func) {
      // summary:
      //      Modifies the given coordinates by the displacement and calls a function to determine the new value.
      var copy = lang.clone(coords);
      for (var i = 0; i < coords.length; i++) {
        for (var j = 0; j < displ.length; j++) {
          copy[i][j] = func(coords[i][j], displ[j], i, j);
        }
      }
      return copy;
    },

    getHeight: function (polygon) {
      // summary:
      //      Returns the average height of the given KmlPolygon.
      var totalHeight = 0;
      var count = 0;
      this.mapPolyCoordsFunc(polygon, function (coord, i) {
        totalHeight += coord.getAltitude();
        count++;
      });
      return totalHeight / count;
    },

    setHeight: function (polygon, height) {
      // summary:
      //      Modifies the height of the given KmlPolygon.
      // description:
      //      This expects polygon.altitudeMode to equal geo.ALTITUDE_RELATIVE_TO_GROUND.
      //      Height should be a numeric string, int or float and is sanitised as a float.

      // TODO this only works for extrusions from the ground (all polygons are set to a certain height)
      height = parseFloat(height);
      this.mapPolyCoordsFunc(polygon, function (coord, i) {
        coord.setAltitude(height);
      });
      this.redraw();
    },

    // TODO(aramk) abandoned this in favour of recreating as a prism with the new altitude
    // At this stage there's way too much overhead to maintain the geometries.
//    setAltitude: function (polygon, altitude) {
//      if (this.isMultiGeometry(polygon.getGeometry())) {
//        console.error('is multi geometry');
//        var outersCoords = this.getOuterBoundariesCoords();
//        var bottomCoords = outersCoords[0];
//        // TODO(aramk) complete rest of this code
//        for (var i = 1; i < outersCoords.length; i++) {
//          var coords = outersCoords[i];
//        }
//      } else {
//        // TODO(aramk) convert the poly into a floating prism
//        Log.error('Cannot set altitude for a non-multi-geometry polygon');
//        console.error('is not multi geometry');
//      }
//    },

    getOuterBoundaryCoords: function (polygon) {
      // summary:
      //      Returns the KmlCoordArray for the outer boundary of a KmlPolygon.
      var geoms = this.mapGeometry(polygon.getGeometry());
      return geoms && geoms.length ? geoms[0].getOuterBoundary().getCoordinates() : null;
    },

    getOuterBoundariesCoords: function (polygon) {
      // summary:
      //      Returns the an array of KmlCoordArray for the outer boundaries of a KmlPolygon.
      var geoms = this.mapGeometry(polygon.getGeometry());
      if (geoms && geoms.length) {
        return array.map(geoms, lang.hitch(this, function (geometry) {
          return geometry.getOuterBoundary().getCoordinates();
        }));
      } else {
        return null;
      }
      return geoms && geoms.length ? geoms[0].getOuterBoundary().getCoordinates() : null;
    },

    getInnerBoundaryCoords: function (polygon) {
      // summary:
      //      Returns the KmlCoordArray for the inner boundary of a KmlPolygon.
      var geoms = this.mapGeometry(polygon.getGeometry());
      return geoms && geoms.length ? geoms[0].getInnerBoundary().getCoordinates() : null;
    },

    getCoordsInDimension: function (/*Number[][]*/ coords, /*int*/ dimension) {
      // summary:
      //      Returns the given coordinates with a subset of their current dimensions.
      dimension = Setter.def(dimension, 3);
      var outer = [];
      for (var i = 0; i < coords.length; i++) {
        outer.push(coords[i].splice(0, dimension));
      }
      return outer;
    },

    getNumberOfCoords: function (poly) {
      var coords = this.getOuterBoundaryCoords(poly);
      return coords ? coords.getLength() : 0;
    },

    // GEOMETRY

    getGeometryCenter: function (geometry) {
      var bounds = this.gex.dom.computeBounds(geometry);
      return bounds ? bounds.center() : null;
    },

    isMultiGeometry: function (geometry) {
      return geometry && geometry.getType() == 'KmlMultiGeometry';
    },

    mapGeometry: function (geometry, callback) {
      // TODO(aramk) add docs
      // summary:
      //    Gets the geometries contained in the given geometry and reeturns an array.
      // callback: Function
      //    If given, this function is called on each geometry returned.
      var geometries = [];
      var toRemove = [];
      if (this.isMultiGeometry(geometry)) {
        var geomObjs = geometry.getGeometries().getChildNodes();
        for (var i = 0; i < geomObjs.getLength(); i++) {
          var geom = geomObjs.item(i);
          geometries.push(geom);
          if (callback) {
            var newGeom = callback(geom, i);
            if (newGeom && newGeom !== geom) {
              geometry.getGeometries().replaceChild(newGeom, geom);
            } else if (!newGeom) {
              toRemove.push(geom);
            }
          }
        }
        for (var j = 0; j < toRemove.length; j++) {
          var removeGeom = toRemove[j];
          geometry.getGeometries().removeChild(removeGeom);
        }
      } else if (geometry) {
        geometries.push(geometry);
        if (callback) {
          callback(geometry);
        }
      }
      return geometries;
    },

    getLines: function (geometry) {
      var geoms = this.mapGeometry(geometry);
      for (var i = 0; i < geoms.length; i++) {
        geoms[i] = geoms[i].getOuterBoundary();
      }
      return geoms;
    },

    getEditableLines: function (geometry) {
      var geoms = this.mapGeometry(geometry);
      // TODO For now we can only edit one linear ring at a time
      return geoms && geoms.length ? [geoms[0].getOuterBoundary()] : geoms;
    },

    _removeUneditableLines: function (geometry) {
      this.mapGeometry(geometry, lang.hitch(this, function (geom, i) {
        return i == 0 ? geom : null;
      }));
    },

    intersects: function (polyA, polyB) {
      // summary:
      //    Checks whether two polygons intersect.
      var coordsA = polyA instanceof Array ? polyA : this.getOuterBoundary(polyA, 2);
      if (!coordsA) {
        return false;
      }
      var wktA = wkt.coordsToWKTObject(coordsA);
      var coordsB = polyB instanceof Array ? polyB : this.getOuterBoundary(polyB, 2);
      if (!coordsB) {
        return false;
      }
      var wktB = wkt.coordsToWKTObject(coordsB);
      return wktA.intersects(wktB);
    },

    // POLYGON COORDINATES

    mapPolyCoordsFunc: function (polygon, func) {
      // summary:
      //      Calls a given function with every KmlCoord and index in a given KmlPolygon outer boundary.
      var coords = this.getOuterBoundaryCoords(polygon);
      if (coords) {
        for (var i = 0; i < coords.getLength(); i++) {
          var coord = coords.get(i);
          if (func) {
            func(coord, i);
          }
          // Note: this is required for changes to take effect
          coords.set(i, coord);
        }
      }
    },

    modifyPolyCoords: function (polygon, displ, func) {
      // summary:
      //      Modifies the given KmlPolygon coordinates using a given function and displacement array.
      this.mapPolyCoordsFunc(polygon, function (coord, i) {
        // This passes more data than you'd usually need - only first two are required
        if (displ.length > 0) {
          coord.setLatitude(func(coord.getLatitude(), displ[0], i, 0, coord));
        }
        if (displ.length > 1) {
          coord.setLongitude(func(coord.getLongitude(), displ[1], i, 1, coord));
        }
        if (displ.length > 2) {
          coord.setAltitude(func(coord.getAltitude(), displ[2], i, 2, coord));
        }
      });
    },

    setPolyCoords: function (polygon, displ) {
      // summary:
      //      Sets the KmlPolygon coordinates.
      this.modifyPolyCoords(polygon, displ, function (x, y) {
        return y;
      });
    },

    addPolyCoords: function (polygon, displ) {
      // summary:
      //      Adds to the KmlPolygon coordinates.
      this.modifyPolyCoords(polygon, displ, function (x, y) {
        return x + y;
      });
    },

    subtractPolyCoords: function (polygon, displ) {
      // summary:
      //      Subtracts to the KmlPolygon coordinates.
      this.modifyPolyCoords(polygon, displ, function (x, y) {
        return x - y;
      });
    },

    movePolyToCenter: function (polygon) {
      // summary:
      //      Moves the given polygon to the center of the screen.
      var lookAt = this.getLookAt();
      var oldCenter = this.getGeometryCenter(polygon.getGeometry());
      // TODO geo.js is imported along with gex, but not explicit import
      var newCenter = new geo.Point(lookAt);
      var diff = new geo.Point(newCenter.lat() - oldCenter.lat(), newCenter.lng() - oldCenter.lng());
      this.addPolyCoords(polygon, [diff.lat(), diff.lng()]);
    },

    // USER INTERACTION

    userDrawPoly: function (args) {
      // summary:
      //      Allows the user to draw a KmlPolygon.
      // description:
      //      The "drawCallback" argument is called on each draw movement.
      //      The "finishCallback" argument is called when drawing is finished.
      var callback = null;
      if (typeof args == 'function') {
        callback = args;
      }
      args = this.merge({
        //                line : poly.getGeometry().getOuterBoundary(), //this.gex.dom.buildLinearRing([]),
        //                polygon : poly,
        polygon: undefined,
        drawCallback: function (index) {
          Log.info('Polygon point drawn');
        },
        finishCallback: function (polygon) {
          Log.info('Polygon drawn');
        }
      }, callback == null ? args : undefined);
      if (callback !== null) {
        args.finishCallback = callback;
      }
      this.isSelectDisabled = true;
      this.isDeselectDisabled = true;
      this.drawObject = null;
      Log.info('Polygon drawing started');
      var placemark = this.drawPoly2D(args.polygon);
      if (!placemark.getGeometry()) {
        var polygon = this.ge.createPolygon('');
        placemark.setGeometry(polygon);
        var ring = this.gex.dom.buildLinearRing([]);
        polygon.setOuterBoundary(ring);
      }
      this.drawObject = placemark;
      var line = placemark.getGeometry().getOuterBoundary();
      this.gex.edit.drawLineString(line, {
        drawCallback: args.drawCallback,
        finishCallback: lang.hitch(this, function () {
          this.isSelectDisabled = false;
          this.isDeselectDisabled = false;
          Log.info('Polygon drawing finished');
          // TODO check coords for dimensions
          var coords = this.getOuterBoundary(placemark);
          args.finishCallback(placemark, coords);
        })
      });
      return args.polygon;
    },

    userStopDraw: function (args) {
      args = args || {};
      var drawObject = this.drawObject;
      if (drawObject) {
        this._userStopEdit(drawObject);
        this.drawObject = null;
        if (args.remove) {
          this.remove(drawObject);
        }
      }
      return drawObject;
    },

    userCancelDraw: function (args) {
      this.userStopDraw({remove: true});
    },

    userEditPoly: function (poly, args) {
      // summary:
      //      Allows the user to edit an existing KmlPolygon.
      // description:
      //      The "callback" is called at the end of editing.
      //      All other properties are assumed to be event IDs, such as click, change, mouseup, etc. with values as callbacks.
      //      These callbacks are called with the polygon as an argument.

      // TODO user timerCallback here

      var callback = null;
      if (typeof args == 'function') {
        callback = args;
      } else {
        args = this.merge({
          callback: undefined
        }, args);
        callback = args.callback;
      }
      Log.info('Polygon edit started', poly, args);
      this.editObject = poly;
      this.finishEditCallback = callback;
      poly.isEditing = true;
      this.isSelectDisabled = true;
      this.isDeselectDisabled = true;
      // This is the default edit callback
      // Note: callback is slow
//      args.lineEditArgs;
      if (args.change) {
        args.lineEditArgs = {
          editCallback: lang.hitch(this, function () {
            this._userEditEvent(poly, args.change);
          })
        };
      }
      this._userStartEdit(poly, args);
      this.editEvents = {};
      array.forEach(this.domEvents, lang.hitch(this, function (eventID) {
        if (eventID in args && eventID != 'change') {
          var eventCallback = lang.hitch(this, function () {
            this._userEditEvent(poly, args[eventID]);
          });
          this.editEvents[eventID] = eventCallback;
          this.addEvent(this.ge.getGlobe(), eventID, eventCallback);
        }
      }));
    },

    _userEditEvent: function (poly, callback) {
      if (callback) {
        callback(poly);
      }
    },

    _userStartEdit: function (poly, args) {
      args = Setter.def(args, {});
      var geom = poly.getGeometry();
      args.lines = args.lines || this.getEditableLines(geom);
      this._removeUneditableLines(geom);
      array.forEach(args.lines, lang.hitch(this, function (line) {
        this.gex.edit.editLineString(line, args.lineEditArgs);
      }));
    },

    userStopEdit: function (poly, args) {
      // summary:
      //      Stops the user editing mode on a given KmlPolygon.
      // description:
      //      The "delay" argument is how long to wait before stopping the edit. Sometimes the object edit mode is not disabled immediately.
      var df = new Deferred();
      args = this.merge({
        delay: 200,
        force: false
      }, args);
      poly = poly || this.editObject;
      if ((!poly || !poly.isEditing) && !args.force) {
        Log.debug('Cannot stop editing since not editing.');
        df.resolve(false);
        return df;
      }
      poly.isEditing = false;
      this.isSelectDisabled = false;
      this.isDeselectDisabled = false;
      this.editObject = null;
      Log.debug('userStopEdit');
      this._userStopEdit(poly, args);
      // XXX This is delayed in case stopping edit lags, adjust if needed
      setTimeout(lang.hitch(this, function () {
        Log.info('Polygon edit stopped', poly);
        if (this.finishEditCallback) {
          // TODO prism support
          this.finishEditCallback(poly, this.getOuterBoundary(poly));
          this.finishEditCallback = null;
          if (this.editEvents) {
            this.removeEvents(this.ge.getGlobe(), this.editEvents);
            this.editEvents = null;
          }
        }
        this.redraw();
        df.resolve(true);
      }), args.delay);
      return df;
    },

    _userStopEdit: function (poly, args) {
      args = Setter.def(args, {});
      args.lines = args.lines || this.getEditableLines(poly.getGeometry());
      array.forEach(args.lines, lang.hitch(this, function (line) {
        this.gex.edit.endEditLineString(line);
      }));
    },

    registerStopEdit: function (/*String?*/ eventID) {
      // summary:
      //      Stops editing the currently editing object when the globe is interacted with.
      // description:
      //      This should be called once.
      eventID = Setter.def(eventID, 'click');
      this.addEvent(this.ge.getGlobe(), eventID, lang.hitch(this, function (event) {
        if (this.editObject != null) {
          Log.debug('Attempting to stop user edit', event, event.getTarget());
          var target = event.getTarget().getType();
          // TODO also stop editing if we click on another polygon - this doesn't work for now because the edit points are also KmlPlacemarks
          var validTarget = event.getTarget().getType() == 'GEGlobe';
          if (!this.polyHasMinCoords(this.editObject)) {
            Log.debug('Polygon not enough vertices');
            var count = this.getNumberOfCoords(this.editObject);
            alert('A polygon must have at least ' + this.minCoords + ' coordinates (including the closing coordinate). This polygon only has ' + count + '.');
          } else if (validTarget && !this.isMousePosChanged(this.mouseDown, this.mouseUp)) {
            Log.debug('pre userStopEdit');
            // Stop if we clicked on the globe ONLY, not if we clicked a polygon or dragged the map
            this.userStopEdit(this.editObject);
            // Reset old data
            this.mouseUp = this.mouseDown = null;
          } else {
            Log.debug('No need to stop user edit');
          }
        }

      }));

      this.addEvent(this.ge.getGlobe(), 'mousedown', lang.hitch(this, function (event) {
        this.mouseDown = event;
      }));

      this.addEvent(this.ge.getGlobe(), 'mouseup', lang.hitch(this, function (event) {
        this.mouseUp = event;
      }));
    },

    userEditToggle: function (poly, args, eventID) {
      // summary:
      //      Assigns a toggle event for editing a KmlPolygon for a given event.
      eventID = Setter.def(eventID, 'click');
      this.addEvent(poly, eventID, lang.hitch(this, function (event) {
        if (!poly.isEditing) {
          this.userEditPoly(poly, args);
        }
      }));

      this.addEvent(this.ge.getGlobe(), eventID, lang.hitch(this, function (event) {
        // TODO prevent this from triggering at the end of a drag
        if (poly.isEditing) {
          this.userStopEdit(poly, null);
        }
      }));
    },

    isMousePosChanged: function (eventA, eventB) {
      // summary:
      //      Checks whether the mouse position changed between two KmlMouseEvent objects.
      return eventA.getClientX() != eventB.getClientX() || eventA.getClientY() != eventB.getClientY();
    },

    polyHasMinCoords: function (poly, min) {
      // summary:
      //      Checks if the given KmlPolygon has at least the minimum number of coordinates.
      min = Setter.def(min, this.minCoords);
      return this.getNumberOfCoords(poly) >= min;
    },

    // OBJECTS

    append: function (obj) {
      // summary:
      //      Adds a KmlObject to the globe.
      if (!obj) {
        Log.warn('Cannot append KML object: ' + obj);
        return;
      }
      this.ge.getFeatures().appendChild(obj);
    },

    remove: function (obj) {
      // summary:
      //      Removes a KmlObject from the globe.
      this.ge.getFeatures().removeChild(obj);
      this.redraw();
    },

    mapChildren: function (callback) {
      var children = this.ge.getFeatures().getChildNodes();
      for (var i = 0; i < children.getLength(); i++) {
        callback(children.item(i));
      }
    },

    getVisibleChildren: function () {
      var visible = [];
      this.mapChildren(lang.hitch(this, function (child) {
        if (child.getVisibility()) {
          visible.push(child);
        }
      }));
      return visible;
    },

    getVisiblePlacemarkChildren: function () {
      return array.filter(this.getVisibleChildren(), function (child) {
        var type = child.getType();
        return type == 'KmlPlacemark'
      });
    },

    // LAYERS

    setLayers: function (layers) {
      // summary:
      //      Sets the given Google Earth layers as being enabled or disabled.
      // layers: Object
      //      The keys should be strings defined here: https://developers.google.com/earth/documentation/layers
      //      The values should be Boolean and indicate if that layer is enabled or not.
      Log.debug('Setting Google Earth layers');
      for (var layer in layers) {
        this.ge.getLayerRoot().enableLayerById(this.ge[layer], !!layers[layer]);
      }
    },

    disableAllLayers: function () {
      // summary:
      //      Disables all Google Earth layers.
      this.setLayers({
        LAYER_BORDERS: false,
        LAYER_BUILDINGS: false,
        LAYER_BUILDINGS_LOW_RESOLUTION: false,
        LAYER_ROADS: false,
        LAYER_TERRAIN: false,
        LAYER_TREES: false
      });
    },

    enableAllLayers: function () {
      // summary:
      //      Enables all Google Earth layers.
      this.setLayers({
        LAYER_BORDERS: true,
        LAYER_BUILDINGS: true,
        LAYER_BUILDINGS_LOW_RESOLUTION: true,
        LAYER_ROADS: true,
        LAYER_TERRAIN: true,
        LAYER_TREES: true
      });
    },

    setLayer: function (layerID, enabled) {
      // summary:
      //      Performs the same task as setLayers() for an individual layer.
      this.ge.getLayerRoot().enableLayerById(this.ge[layerID], enabled);
    },

    getLayer: function (layerID) {
      // summary:
      //      Returns a layer as a KmlFolder.
      return this.ge.getLayerRoot().getLayerById(layerID);
    },

    isGELayerVisible: function (layerID) {
      // summary:
      //      Returns true if the given layer is visible.
      var layer = this.getLayer(this.ge[layerID]);
      if (layer) {
        return layer.getVisibility();
      } else {
        return false;
      }
    },

    // KMZ

    isKMZLayerVisible: function (layerID) {
      // summary:
      //      Returns true if the given KMZ layer is visible.
      return layerID in this.layers;
    },

    showKMZLayer: function (layerID, url, args) {
      // summary:
      //      Displays a KMZ object from a given url as a layer.
      args = args || {};
      var oldCallback = args.callback;
      var callback = lang.hitch(this, function (kmlObject) {
        this.setKMZLayer(layerID, kmlObject);
        this.redraw();
        if (oldCallback) {
          oldCallback(layerID, kmlObject);
        }
      });
      var existing = this.getKMZLayer(layerID);
      if (existing) {
        callback(existing);
      } else {
        try {
          Log.info('Showing layer', layerID, url, args);
          // TODO check if ID exists
          this.displayKml(url, lang.mixin(args, {
            callback: callback
          }));
        } catch (err) {
          Log.warn('Could not display kml', err, layerID, url);
        }
      }
    },

    hideKMZLayer: function (layerID) {
      // summary:
      //      Hides a previously displayed KMZ layer.
      var layer = this.getKMZLayer(layerID);
      Log.info('Hiding layer', layerID, layer);
      if (layer) {
        this.remove(layer);
      }
    },

    setKMZLayer: function (id, layer) {
      this.layers[id] = layer;
      this.append(layer);
    },

    getKMZLayer: function (id) {
      return this.layers[id]
    },

    displayKml: function (url, args) {
      // summary:
      //      Displays a KMZ file given a url.
      // description:
      //      Adapted from GEarthExtensions.util to allow a callback when displaying is complete.
      args = this.merge({
        cacheBuster: false,
        flyToView: false,
        flyToBoundsFallback: true,
        aspectRatio: 1.0,
        callback: null
      }, args);

      if (args.cacheBuster) {
        url += (url.match(/\?/) ? '&' : '?') + '_cacheBuster=' +
            Number(new Date()).toString();
      }

      Log.info('Fetching kml', url);
      google.earth.fetchKml(this.ge, url, lang.hitch(this, function (kmlObject) {
        if (kmlObject) {
          Log.info('Fetched kml', url);
          this.append(kmlObject);
          if (args.callback) {
            args.callback(kmlObject);
          }
          if (args.flyToView) {
            this.gex.util.flyToObject(kmlObject, {
              boundsFallback: args.flyToBoundsFallback,
              aspectRatio: args.aspectRatio
            });
          }
        }
      }));
    },

    // BALLOONS

    createHTMLBalloon: function (feature, args) {
      // summary:
      //      Creates an HTML balloon and attaches it to a KmlFeature.
      return this.createBalloon(feature, 'html', args);
    },

    createFeatureBalloon: function (feature, args) {
      // summary:
      //      Creates a feature balloon and attaches it to a KmlFeature.
      return this.createBalloon(feature, 'feature', args);
    },

    createDivBalloon: function (feature, args) {
      // summary:
      //      Creates a DIV balloon and attaches it to a KmlFeature.
      return this.createBalloon(feature, 'div', args);
    },

    createBalloon: function (feature, type, args) {
      // summary:
      //      Creates a balloon of a given type and attaches it to a KmlFeature.
      // type: String
      //      Either "html", "feature" or "div" as per https://developers.google.com/earth/documentation/balloons
      // description:
      //      Argument "content" (String) defines HTML content for the "html" and "div" types. The "feature" type gets its content from the description in the KmlFeature.
      //      Argument "minHeight", "maxHeight", "minWidth" and "maxHeight" (Number) set dimensions.
      //      Argument "closeButton" (Boolean) shows a close button if true.
      args = this.merge({
        content: undefined,
        minHeight: 400,
        maxHeight: 400,
        minWidth: 400,
        maxWidth: 400,
        closeButton: true
      }, args);

      var balloon;
      if (type == 'feature') {
        balloon = this.ge.createFeatureBalloon('');
      } else if (type == 'div') {
        balloon = this.ge.createHtmlDivBalloon('');
        if (typeof args.content != 'undefined') {
          balloon.setContentDiv(args.content);
        }
      } else if (type == 'html') {
        balloon = this.ge.createHtmlStringBalloon('');
        if (typeof args.content != 'undefined') {
          balloon.setContentString(args.content);
        }
      } else {
        return null;
      }

      if (typeof feature != 'undefined') {
        balloon.setFeature(feature);
      }
      balloon.setMinHeight(args.minHeight);
      balloon.setMaxHeight(args.maxHeight);
      balloon.setMinWidth(args.minWidth);
      balloon.setMaxWidth(args.maxWidth);
      balloon.setCloseButtonEnabled(args.closeButton);
      return balloon;
    }

    // TODO for color see http://code.google.com/p/earth-api-utility-library/wiki/GEarthExtensionsUtilReference

  });
});
