// TODO this is an orphaned class in some sense since I've used it in three projects now, but we don't need the other classes in either so I've used it individually

define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'utility/Log'
], function (declare, lang, Log) {
  return {
    // summary:
    //      Provides methods for creating instances of modules

    declareGlobal:function () {
      // summary:
      //      Declares a global instance and returns the declared module as per normal.
      // description:
      //      The global instance is accessible with getInstance(). This function should be called in place of dojo.declare().
      var cls = declare.apply(this, arguments);
      return this.globalise(cls);
    },

    globalise:function (cls) {
      // summary:
      //      Declares a global instance and returns the declared module as per normal for the given class.
      // description:
      //      This should be called with a returned class from dojo.declare().
      cls._instance = null;
      cls.getInstance = function () {
        if (!cls.prototype._instance) {
          cls.prototype._instance = new cls();
        }
        return cls.prototype._instance;
      };
      var proto = cls.prototype;
      // These must never be overridden - there is no risk of doing so in any case (see below)
      var reserved = {
        'constructor':null,
        'getInherited':null,
        '__inherited':null,
        'inherited':null,
        'isInstanceOf':null
      }
      for (var prop in proto) {
        if (!(prop in reserved) && dojo.isFunction(proto[prop])) {
          if (prop in cls) {
            // Do not override an existing property
            Log.error('Could not assign function "' + prop + '"', cls);
          } else {
            // Create a closure to ensure func is defined as the current "prop" and not the one available outside the for loop.
            (function (func) {
              cls[func] = lang.hitch(cls.getInstance(), func);
            })(prop);
          }
        }
      }
      return cls;
    }

  };
});
