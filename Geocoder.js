define([
  'dojo/_base/declare',
  'dojo/_base/lang',
  'dojo/Deferred',
  './wkt/Extensions',
  './manager/InstanceManager',
  './lib/google.jsapi',
  'utility/Log'
], function (declare, lang, Deferred, wkt, InstanceManager, jsapi, Log) {

  return InstanceManager.declareGlobal(null, {
    // summary:
    //      Geocoder module which provides method for normalising location strings and converting them to coordinates.
    // description:
    //      See https://developers.google.com/maps/documentation/geocoding/

    // The static instance of this class
    instance: null,
    // Used to delay methods until Geocoder is loaded
    _loadDf: null,
    // The geocoder instance
    geocoder: null,

    constructor: function (geocoder) {
      // summary:
      //      Creates an instance of the plugin using a given Google Map Geocoder.
      // description:
      //      If none is specified, The Google Maps library is loaded and a Geocoder instance if created.
      this._loadDf = new Deferred();

      if (typeof geocoder != 'undefined') {
        this.geocoder = geocoder;
        this._loadDf.resolve("success");
      } else {
        jsapi(lang.hitch(this, function () {
          google.load('maps', '3.6', {
            other_params: 'sensor=false',
            callback: lang.hitch(this, function () {
              Log.info('Loaded Google Maps');
              this.geocoder = new google.maps.Geocoder();
              this._loadDf.resolve("success");
            })
          });
        }));
      }

    },

    geocode: function (/*String*/ address) {
      // summary:
      //      Geocodes a given address and calls a function with the result and status.
      var df = new Deferred(),
          reject = function (err) {
            df.reject(err);
          };
      this._loadDf.then(lang.hitch(this, function () {
        this.geocoder.geocode({'address': address}, function (results, status) {
          if (status != google.maps.GeocoderStatus.OK) {
            Log.warn("Could not determine location", address, status);
          }
          df.resolve({
            results: results,
            status: status
          });
        });
      }), reject);
      return df;
    },

    getInfo: function (/*String*/ address) {
      // summary:
      //      Geocodes an adress and calls a function with the results.
      // description:
      //       The results has the property "success". If this is true, it contains the standardised location name ("name"), coordinate ("coords") and WKT point ("point").
      var df = new Deferred();
      this.geocode(address).then(lang.hitch(this, function (args) {
        var results = args.results,
            success = results.length > 0,
            result = {success: success};
        if (success) {
          result.name = this.getProperty(results[0], 'long_name');
          result.coords = this.getProperty(results[0], 'location');
          result.point = "POINT (" + result.coords.lat + " " + result.coords.lng + ")"; // "POINT (20 20)";
        }
        df.resolve(result);
      }));
      return df;
    },

    getProperty: function (/*Object*/ result, /*String*/ property) {
      // summary:
      //      Returns property names from the results of a geocode.
      // description:
      //      Accepts "long_name" and "location" as the property.
      if (property == 'long_name') {
        var name = '';
        result.address_components.forEach(function (part, i) {
          if (i != 0) {
            name += ', ';
          }
          name += part.long_name;
        });
        return name;
      } else if (property == 'location') {
        var loc = result.geometry.location;
        return {lat: loc.lat(), lng: loc.lng()};
      }
    }

  });

});
