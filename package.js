/**
 * This file determines build parameters for Earth.
 */

var testRegex = /^earth\/tests\/?/;

var copyOnly = function (filename, mid) {
  return mid in {
    "earth/package.json": 1,
    "earth/tests": 1
  };
};

var profile = {
  resourceTags: {
    amd: function (filename, mid) {
      return !copyOnly(filename, mid) && /\.js$/.test(filename) &&
          !/earth\/lib\/extensions/.test(mid);
    },

    copyOnly: function (filename, mid) {
      return copyOnly(filename, mid);
    },

    // Use mini: true in profile to exclude test files in preference to the test tag and copyTests.
    miniExclude: function (filename, mid) {
      return mid === './package' || /^earth\/sample\//.test(mid) || testRegex.test(mid);
    },

    test: function (filename, mid) {
      return testRegex.test(mid);
    }
  }
};
